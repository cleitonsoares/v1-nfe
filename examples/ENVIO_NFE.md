Enviar uma NFe para Receita:

POST em http://localhost:8081/v1-nfe/nfe

Content-Type:application/json

REQUEST PAYLOAD "BODY": {
                            "destinatario": {
                                "cliFor": "2",
                                "xNomeDest": "Associaçãoo social do Mosteiro Nossa Senhora da Paz",
                                "cpfcnpjDest": "17.285.556/0001-74",
                                "IEDest": "002069362.00-05",
                                "IMunDest": "",
                                "tipContribuinte": 1,
                                "emailDest": "",
                                "foneDest": "3432323232",
                                "CEPDest": "38400110",
                                "xLgrDest": "Tenente Virmondes",
                                "nroDest": "1400",
                                "xCplDest": null,
                                "xBairroDest": "Centro",
                                "xMunDest": "Uberlândia",
                                "UFDest": "MG",
                                "cMunDest": "3170206",
                                "exterior": 0,
                                "cPaisDest": "1058",
                                "xPaisDest": "Brasil"
                            },
                            "nota": {
                                "serie": "1",
                                "nNF": "2000",
                                "vProd": "651.56",
                                "vNF": "651.56",
                                "vOutro": "0.00",
                                "avAp": 1,
                                "chave": null,
                                "refNFe": "",
                                "natOp": "Venda de produtos adquiridos de terceiros",
                                "codNatOp": 1,
                                "vDesc": "0.00",
                                "vFrete": "0.00",
                                "vSeg": "0.00",
                                "cfop": 5101,
                                "infCpl": "IPI SUSPENSO CONFORME ARTIGO 29 DA LEI N 10.637/02 DISCIPLINADO PELO ART 5 DA IMR-SRF 948/09;",
                                "modFrete": 0,
                                "dhEmi": "2018-08-17 00:00:00",
                                "dhSaiEnt": "2018-08-17 11:00:00",
                                "finNFe": 1,
                                "tPag": "Boleto",
                                "itens": [{
                                    "nItem": 1,
                                    "cProd": 6048,
                                    "orig": 0,
                                    "xProd": "APRENDA MUSICA DE OUVIDO VOL 2",
                                    "qCom": "1.0000",
                                    "vUnCom": "651.5600",
                                    "vProd": "651.56",
                                    "vFrete": "0.0000",
                                    "vOutro": "0.0000",
                                    "vSeg": "0.0000",
                                    "CFOP": 5101,
                                    "infAdProd": null,
                                    "xPed": "0",
                                    "nItemPed": "0",
                                    "NCM": "85232990",
                                    "cEAN": "",
                                    "uCom": "UN",
                                    "EXTIPI": "",
                                    "CEST": "",
                                    "CNPJFab": null,
                                    "cBenef": "46556456",
                                    "vBCFCPST": null,
                                    "CST": "00",
                                    "modBC": 3,
                                    "vBC": 651.56,
                                    "pICMS": 18,
                                    "vICMS": 117.2808,
                                    "pFCP": null,
                                    "vFCP": null,
                                    "vBCFCP": null,
                                    "modBCST": 0,
                                    "pMVAST": 0,
                                    "pRedBCST": 0.00,
                                    "vBCST": 0.00,
                                    "pICMSST": 0.00,
                                    "vICMSST": 0.00,
                                    "pFCPST": 0.00,
                                    "vFCPST": 0.00,
                                    "vICMSDeson": 0.00,
                                    "motDesICMS": "",
                                    "pRedBC": 0.00,
                                    "vICMSOp": "",
                                    "pDif": "",
                                    "vICMSDif": "",
                                    "vBCSTRet": "",
                                    "pST": "",
                                    "vICMSSTRet": "",
                                    "vBCFCPSTRet": "",
                                    "pFCPSTRet": "",
                                    "vFCPSTRet": "",
                                    "pDevol": "",
                                    "vIPIDevol": "",
                                    "cstIPI": "04",
                                    "cEnq": "146",
                                    "vIPI": "",
                                    "pIPI": "",
                                    "pPIS": "",
                                    "vPIS": "",
                                    "qBCProd": "",
                                    "vAliqProd": "",
                                    "pCOFINS": "",
                                    "vCOFINS": "",
                                    "vBCUFDest": "",
                                    "vBCFCPUFDest": "",
                                    "pFCPUFDest": "",
                                    "pICMSUFDest": 0.00,
                                    "pICMSInter": "",
                                    "pICMSInterPart": "",
                                    "vFCPUFDest": "",
                                    "vICMSUFDest": "",
                                    "vICMSUFRemet": ""
                                }],
                                "parcelas": [{
                                        "nDup": 1,
                                        "vDup": "130.28",
                                        "dVenc": "2018-08-11 00:00:00"
                                    },
                                    {
                                        "nDup": 2,
                                        "vDup": "130.32",
                                        "dVenc": "2018-09-11 00:00:00"
                                    },
                                    {
                                        "nDup": 3,
                                        "vDup": "130.32",
                                        "dVenc": "2018-10-11 00:00:00"
                                    },
                                    {
                                        "nDup": 4,
                                        "vDup": "130.32",
                                        "dVenc": "2018-11-11 00:00:00"
                                    },
                                    {
                                        "nDup": 5,
                                        "vDup": "130.32",
                                        "dVenc": "2018-12-11 00:00:00"
                                    }
                                ],
                                "transporte": {
                                    "placaVeic": "AAA1234",
                                    "siglaUFVeic": "AL",
                                    "qVolTransp": 100,
                                    "espTransp": "100",
                                    "nVolTransp": null,
                                    "pesoLTransp": "230.000",
                                    "pesoBTransp": "230.000"
                                },
                                "transportadora": {
                                    "pfPj": "2",
                                    "xNomeTransp": "Empresa Brasileira De Correios E Telegrafos",
                                    "cpfcnpjTransp": "34028316002661",
                                    "numIETransp": null,
                                    "end_cep": "68881500",
                                    "xEnderTransp": "Estrada Mosteiro Nossa Senhora da Paz",
                                    "nroTransp": "1400",
                                    "xCplTransp": null,
                                    "xBairroTransp": "Potuverao",
                                    "xMunTransp": "Itapecerica da Serra",
                                    "siglaUF": "SP"
                                }
                            }
                        }


EXEMPLO DE RETORNO NOTA AUTORIZADA: {
                                        "status": 200,
                                        "href": "http://localhost:8081/erp/nfe?fields=&search=()",
                                        "first": false,
                                        "previous": false,
                                        "next": "http://localhost:8081/erp/nfe?offset=1&fields=&search=()",
                                        "count": 7,
                                        "limit": 0,
                                        "offset": 0,
                                        "sort": "",
                                        "direction": "",
                                        "search": "",
                                        "fields": "",
                                        "records": {
                                            "codSefaz": "100",
                                            "strMotivo": "Autorizado o uso da NF-e",
                                            "msg": "Documento autorizado",
                                            "error": false,
                                            "chave": "31180711346035000101550010000000461000000461"
                                        }
                                    }


CONSULTANDO NFE:

GET em http://localhost:8081/v1-nfe/nfe/?q=(chave:31180711346035000101550010000000471000000477,recibo:54556465,apenasconsulta:0)
pode ser passado um ou ambos.
apenasconsulta vai ser 0 para consuta simples ou 1 para adicionar o protocolo no xml caso esteja autorizado.