ESTRUTURA BÁSICA DO DIRETÓRIO /var/www/nfe:

├── certs
├── danfe
├── extras
├── homologacao
│   ├── aprovadas
│   ├── assinadas
│   ├── canceladas
│   │   └── 201807
│   ├── cartacorrecao
│   │   └── 201807
│   ├── dfe
│   │   ├── protocolados
│   ├── entradas
│   ├── enviadas
│   │   └── aprovadas
│   │       └── 201807
│   ├── recebidas
│   │   └── 201807
│   └── temporarias
│       └── 201807

O APACHE DEVE TER PERMISSÃO DE LEITURA E GRAVAÇÃO NESSE DIRETÓRIO

O CERTIFICADO DIGITAL DEVE SER ALOCADO NA PASTA certs E SEU NOME DEVE SER INFORMADO NO CONSTRUTOR DA CLASSE NFE
JUNTAMENTE COM OS DADOS DA EMPRESA

