GET EM http://localhost/v1/nfe/dfe

Esse processo irá baixar o resumo das notas fiscais emitidas contra seu CNPJ;
Esse processo armazena os resumos na pasta dfe/, caso já exista xmls manifestados, grava em /protocolados;
O retorno deve ser persistido no banco para que as notas sejam manifestadas e assim possam ter seus xmls protocolados disponíveis para download;

MANIFESTANDO UMA NOTA:
POST EM http://localhost/v1/nfe/dfe

---- CÓDIGOS DOS EVENTOS:
210200 Evento confirmação;
210210 Evento ciencia da operação;
210220 Evento de Operação não Realizada
210240 Evento não realizada; - NESSE EVENTO, A JUSTIFICATIVA É OBRIGATÓRIA

REQUEST PAYLOAD "BODY": [
                        	{
                        		"chave": "02345678901234567890123456789012345678901234",
                        		"cod_evento": 210210,
                        		"justificativa": "",
                        		"seq": 1
                        	},{
                        		"chave": "82345678901234567890123456789012345678901234",
                        		"cod_evento": 210200,
                        		"justificativa": "",
                        		"seq": 1
                        	},{
                        		"chave": "72345678901234567890123456789012345678901234",
                        		"cod_evento": 210210,
                        		"justificativa": "",
                        		"seq": 1
                        	},{
                        		"chave": "72345678901234567890123456789012345678901234",
                        		"cod_evento": 210240,
                        		"justificativa": "Entrega de produto não solicitado.",
                        		"seq": 2
                        	}
                        ]


EXEMPLO DE RETORNO CANCELAMENTO: {
                                     "status": 200,
                                     "href": "http://localhost/nfe/dfe?fields=&search=()",
                                     "first": false,
                                     "previous": false,
                                     "next": "http://localhost/nfe/dfe?offset=1&fields=&search=()",
                                     "count": 4,
                                     "limit": 0,
                                     "offset": 0,
                                     "sort": "",
                                     "direction": "",
                                     "search": "",
                                     "fields": "",
                                     "records": [
                                         {
                                             "codSefaz": "135",
                                             "strMotivo": "Evento registrado e vinculado a NFe",
                                             "msg": "Rejeicao: Chave de Acesso com digito verificador invalido",
                                             "error": 1,
                                             "chave": "02345678901234567890123456789012345678901234"
                                         },
                                         {
                                             "codSefaz": "236",
                                             "strMotivo": "Rejeicao: Chave de Acesso com digito verificador invalido",
                                             "msg": "Rejeicao: Chave de Acesso com digito verificador invalido",
                                             "error": 1,
                                             "chave": "82345678901234567890123456789012345678901234"
                                         },
                                         {
                                             "codSefaz": "236",
                                             "strMotivo": "Rejeicao: Chave de Acesso com digito verificador invalido",
                                             "msg": "Rejeicao: Chave de Acesso com digito verificador invalido",
                                             "error": 1,
                                             "chave": "72345678901234567890123456789012345678901234"
                                         },
                                         {
                                             "codSefaz": "236",
                                             "strMotivo": "Rejeicao: Chave de Acesso com digito verificador invalido",
                                             "msg": "Rejeicao: Chave de Acesso com digito verificador invalido",
                                             "error": 1,
                                             "chave": "72345678901234567890123456789012345678901234"
                                         }
                                     ]
                                 }

Após esse evento, caso o evento seja vinculado a NF-e, basta fazer o download unitário desse xml acesse o endpoint pelo navegador que o xml será baixado:
http://localhost/v1/nfe/dfe/02345678901234567890123456789012345678901234