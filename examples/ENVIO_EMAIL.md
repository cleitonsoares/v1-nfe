Enviar um Email com a Nota:

OBS: A situação determina qual documento será enviado por email, caso exista carta de correção,
este é o documento que será enviado.

POST em http://localhost:8081/v1-nfe/nfe/email

Content-Type:application/json

REQUEST PAYLOAD "BODY": {
                            "emails": "cleitonhjs@hotmail.com", /* separar emails por virgula */
                            "chave":"31180711346035000101550010000000491000000498",
                            "situacao":"autorizado"
                        }

EXEMPLO DE RETORNO CANCELAMENTO: {
                                     "status": 200,
                                     "href": "http://localhost:8081/erp/nfe/email?fields=&search=()",
                                     "first": false,
                                     "previous": false,
                                     "next": "http://localhost:8081/erp/nfe/email?offset=1&fields=&search=()",
                                     "count": 3,
                                     "limit": 0,
                                     "offset": 0,
                                     "sort": "",
                                     "direction": "",
                                     "search": "",
                                     "fields": "",
                                     "records": {
                                         "title": "Sucesso!",
                                         "error": false,
                                         "msg": "Email enviado com Sucesso!"
                                     }
                                 }


EXEMPLO DE RETORNO COM ERRO: {
                                 "status": 200,
                                 "href": "http://localhost:8081/erp/nfe/email?fields=&search=()",
                                 "first": false,
                                 "previous": false,
                                 "next": "http://localhost:8081/erp/nfe/email?offset=1&fields=&search=()",
                                 "count": 3,
                                 "limit": 0,
                                 "offset": 0,
                                 "sort": "",
                                 "direction": "",
                                 "search": "",
                                 "fields": "",
                                 "records": {
                                     "title": "Erro!",
                                     "error": true,
                                     "msg": "Falha: A mensagem não pode ser enviada. Mail Error: SMTP Error: data not accepted.SMTP server error: DATA END command failed Detail: Message rejected: Email address is not verified. The following identities failed the check in region US-WEST-2: root@localhost, Root User <root@localhost>\r\n SMTP code: 554"
                                 }
                             }

