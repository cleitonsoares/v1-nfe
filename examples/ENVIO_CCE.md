Enviar uma NFe para Receita:

POST em http://localhost:8081/v1-nfe/nfe/cce

Content-Type:application/json

REQUEST PAYLOAD "BODY": {
                            "cce_descricao": "CORRIGIDO A PARA B",
                            "cce_seq": 2,
                            "chave":"31180711346035000101550010000000471000000477"
                        }

EXEMPLO DE RETORNO CCE AUTORIZADA: {
                                       "status": 200,
                                       "href": "http://localhost:8081/erp/nfe/cce?fields=&search=()",
                                       "first": false,
                                       "previous": false,
                                       "next": "http://localhost:8081/erp/nfe/cce?offset=1&fields=&search=()",
                                       "count": 3,
                                       "limit": 0,
                                       "offset": 0,
                                       "sort": "",
                                       "direction": "",
                                       "search": "",
                                       "fields": "",
                                       "records": {
                                           "title": "Sucesso!",
                                           "error": false,
                                           "msg": [
                                               "Solicitação de envio de carta de correção enviada para Sefaz e retornada com o seguinte status: 135(Evento registrado e vinculado a NF-e.)"
                                           ]
                                       }
                                   }


EXEMPLO DE RETORNO CCE ERRO: {
                                 "status": 200,
                                 "href": "http://localhost:8081/erp/nfe/cce?fields=&search=()",
                                 "first": false,
                                 "previous": false,
                                 "next": "http://localhost:8081/erp/nfe/cce?offset=1&fields=&search=()",
                                 "count": 3,
                                 "limit": 0,
                                 "offset": 0,
                                 "sort": "",
                                 "direction": "",
                                 "search": "",
                                 "fields": "",
                                 "records": {
                                     "title": "Erro!",
                                     "error": true,
                                     "msg": "Solicitação de envio de carta de correção enviada para Sefaz e retornada com o seguinte status: 573(Rejeicao: Duplicidade de Evento)"
                                 }
                             }

