Enviar uma NFe para Receita:

PUT em http://localhost:8081/v1-nfe/nfe

Content-Type:application/json

REQUEST PAYLOAD "BODY": {
                            "justificativa": "ERRO NA DIGITAÇÃO DOS DADOS",
                            "chave":"31180711346035000101550010000000471000000477"
                        }

EXEMPLO DE RETORNO CANCELAMENTO: {
                                       "status": 200,
                                       "href": "http://localhost:8081/erp/nfe?fields=&search=()",
                                       "first": false,
                                       "previous": false,
                                       "next": "http://localhost:8081/erp/nfe?offset=1&fields=&search=()",
                                       "count": 5,
                                       "limit": 0,
                                       "offset": 0,
                                       "sort": "",
                                       "direction": "",
                                       "search": "",
                                       "fields": "",
                                       "records": {
                                           "codSefaz": "135",
                                           "strMotivo": "Documento cancelado. Evento registrado e vinculado a NF-e.",
                                           "msg": "Documento cancelado. Evento registrado e vinculado a NF-e.",
                                           "error": false,
                                           "chave": "31180711346035000101550010000000471000000477"
                                       }
                                   }

