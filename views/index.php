<?php

use Phalcon\Mvc\View;

$view = new View();
$view->setViewsDir(__DIR__);
$view->start();
$view->render("nfe", "interface");
$view->finish();

echo $view->getContent();
