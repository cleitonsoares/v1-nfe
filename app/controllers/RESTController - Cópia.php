<?php

namespace EmissorNfe\Controllers;

use EmissorNfe\Exceptions\HTTPException,
    EmissorNfe\Helpers\Log as Log,
    EmissorNfe\Helpers\Formata;
use EmissorNfe\Psico\Models\Auditoria;

header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Headers: *');

/**
 * Base RESTful Controller.
 * Supports queries with the following paramter s:
 *   Searching:
 *     q=(searchField1:value1,searchField2:value2)
 *   Partial Responses:
 *     fields=(field1,field2,field3)
 *   Limits:
 *     limit=10
 *   Partials:
 *     offset=20
 *
 */
class RESTController extends \EmissorNfe\Controllers\BaseController{

    /**
     * If query string contains 'q' parameter.
     * This indicates the request is searching an entity
     * @var boolean
     */
    protected $isSearch = false;

    /**
     * If query contains 'fields' parameter.
     * This indicates the request wants back only certain fields from a record
     * @var boolean
     */
    protected $isPartial = false;

    /**
     * Set when there is a 'limit' query parameter
     * @var integer
     */
    protected $limit = null;

    /**
     * Set when there is an 'offset' query parameter
     * @var integer
     */
    protected $offset = null;

    /**
     * Array of fields requested to be searched against
     * @var array
     */
    protected $searchFields = null;

    /**
     * Array of fields requested to be returned
     * @var array
     */
    protected $partialFields = null;

    protected $sort = null;

    protected $direction = null;

    protected $expired = null;

    /**
     * Sets which fields may be searched against, and which fields are allowed to be returned in
     * partial responses.  This will be overridden in child Controllers that support searching
     * and partial responses.
     * @var array
     */
    protected $allowedFields = array(
        'search' => array(),
        'partials' => array()
    );

    public $identEmp;
    public $decodedJwt;
    public $arrHeaderToken;
    public $solucao;
    public $moduloLog;
    public $arrModPerm;

    /**
     * Constructor, calls the parse method for the query string by default.
     * @param boolean $parseQueryString true Can be set to false if a controller needs to be called
     *        from a different controller, bypassing the $allowedFields parse
     * @return void
     */
    public function __construct($parseQueryString = true){

        $this->arrHeaderToken = [];
        
        parent::__construct();

        if ($parseQueryString){
            $this->parseRequest($this->allowedFields);
        }

        return;
    }

    public function salvarLog($params=array())
    {
        $desc = '';
        if(is_array($params)) {
            foreach ($params as $key => $value)
            {
                if($key == 'desc') {
                    $desc = utf8_encode($value);
                }
            }
        } else {
            $desc = $params;
        }

        Log::setLog($desc);
    }

    public function setLog($string)
    {
        $fp = fopen(PATH_LOGS . 'log-nfe', 'a+');
        $string = (!is_array($string))?$string:print_r($string,true);
        fwrite($fp, date('d-m-Y H:i:s').$string."\r\n");
        fclose($fp);
    }

    /**
     * Transforma de (name:Benjamin Framklin,location:Philadelphia) para
     * array('name'=>'Benjamin Franklin', 'location'=>'Philadelphia')
     * Parses out the search parameters from a request.
     * Unparsed, they will look like this:
     *    (name:Benjamin Framklin,location:Philadelphia)
     * Parsed:
     *     array('name'=>'Benjamin Franklin', 'location'=>'Philadelphia')
     * @param  string $unparsed Unparsed search string
     * @return array            An array of fieldname=>value search parameters
     */
    protected function parseSearchParameters($unparsed){

        // Strip parens that come with the request string
        $unparsed = trim($unparsed, '()');

        // Now we have an array of "key:value" strings.
        $splitFields = explode(',', $unparsed);
        $mapped = array();

        // Split the strings at their colon, set left to key, and right to value.
        foreach ($splitFields as $field) {
            $splitField = explode(':', $field);
            $mapped[$splitField[0]] = $splitField[1];
        }

        return $mapped;
    }

    /**
     * Parses out partial fields to return in the response.
     * Unparsed:
     *     (id,name,location)
     * Parsed:
     *     array('id', 'name', 'location')
     * @param  string $unparsed Unparsed string of fields to return in partial response
     * @return array            Array of fields to return in partial response
     */
    protected function parsePartialFields($unparsed){
        return explode(',', trim($unparsed, '()'));
    }

    /**
     * Main method for parsing a query string.
     * Finds search paramters, partial response fields, limits, and offsets.
     * Sets Controller fields for these variables.
     *
     * @param  array $allowedFields Allowed fields array for search and partials
     * @return boolean              Always true if no exception is thrown
     */
    protected function parseRequest($allowedFields){
        $request = $this->di->get('request');
        //print_r($request); die;
        $searchParams = $request->get('q', null, null);
        $fields = $request->get('fields', null, null);

        // Set limits and offset, elsewise allow them to have defaults set in the Controller
        $this->limit = ($request->get('limit', null, null)) ?: $this->limit;
        $this->offset = ($request->get('offset', null, null)) ?: $this->offset;

        // If there's a 'q' parameter, parse the fields, then determine that all the fields in the search
        // are allowed to be searched from $allowedFields['search']
        if($searchParams){
            $this->isSearch = true;
            $this->searchFields = $this->parseSearchParameters($searchParams);

            // This handly snippet determines if searchFields is a strict subset of allowedFields['search']
            if(array_diff(array_keys($this->searchFields), $this->allowedFields['search'])){

                throw new HTTPException(
                    "The fields you specified cannot be searched.",
                    401,
                    array(
                        'dev' => 'You requested to search fields that are not available to be searched.',
                        'internalCode' => 'S1000',
                        'more' => '' // Could have link to documentation here.
                    ));
            }
            //die('555');
        }

        // If there's a 'fields' paramter, this is a partial request.  Ensures all the requested fields
        // are allowed in partial responses.
        if($fields){
            $this->isPartial = true;
            $this->partialFields = $this->parsePartialFields($fields);

            //print_r($this->allowedFields); die;

            // Determines if fields is a strict subset of allowed fields
            if(array_diff($this->partialFields, $this->allowedFields['partials'])){
                throw new HTTPException(
                    "The fields you asked for cannot be returned.",
                    401,
                    array(
                        'dev' => 'You requested to return fields that are not available to be returned in partial responses.',
                        'internalCode' => 'P1000',
                        'more' => '' // Could have link to documentation here.
                    ));
            }
        }

        return true;
    }

    /**
     * Provides a base CORS policy for routes like '/users' that represent a Resource's base url
     * Origin is allowed from all urls.  Setting it here using the Origin header from the request
     * allows multiple Origins to be served.  It is done this way instead of with a wildcard '*'
     * because wildcard requests are not supported when a request needs credentials.
     *
     * @return true
     */
    public function optionsBase(){

        //Log::setLog('optionssssssssssssssbaseeeeeeeeeeeeeee: ');

        //die('ijijijijjiji');

        $response = $this->di->get('response');
        $arrRequest = $this->di->get('getHttpRequest');

        //Log::setLog('headerrrrrrrrrrrrrrsssssssssssssssss: '.print_r($arrRequest,true));

        $origin = (isset($arrRequest['Origin']) ? $arrRequest['Origin'] : '*');

        $response->setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//        $response->setHeader('Access-Control-Request-Method', 'GET,PUT,POST,DELETE,OPTIONS');

        $response->setHeader('Access-Control-Allow-Origin', $origin);
        $response->setHeader('Content-Type', 'application/json');

        //Requested-With,Content-Type,Accept,Authorization,Accept-Language,Content-Language,Last-Event-ID,X-HTTP-Method-Override
        $response->setHeader('Access-Control-Allow-Headers','accept,authorization,content-type,Autorizacaoteste,Origin,origin,Requested-With,Accept-Language,Last-Event-ID,X-HTTP-Method-Override,cache-control,x-requested-with'); //'accept,authorization,content-type');//$this->_listHeaders());//'accept,authorization,content-type'); //$this->_listHeaders()); //'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization');// $this->_listHeaders());
        $response->setHeader('Access-Control-Allow-Credentials', 'true');

//        $response->setHeader('Access-Control-Request-Headers', 'accept,authorization');

        $response->setHeader('Access-Control-Max-Age', '3600');
        return true;
    }

    /**
     * Provides a CORS policy for routes like '/users/123' that represent a specific resource
     *
     * @return true
     */
    public function optionsOne(){

        //Log::setLog('optionssssssssssssssoneeeee: ');
        $response = $this->di->get('response');
        $response->setHeader('Access-Control-Allow-Methods', 'GET, PUT, PATCH, DELETE, OPTIONS, HEAD');
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Content-Type', 'application/json');
        //$response->setHeader('Access-Control-Allow-Credentials', 'true');
        $response->setHeader('Access-Control-Allow-Headers', $this->_listHeaders());
        $response->setHeader('Access-Control-Max-Age', '86400');
        return true;
    }

    /**
     * Should be called by methods in the controllers that need to output results to the HTTP Response.
     * Ensures that arrays conform to the patterns required by the Response objects.
     *
     * @param  array $recordsArray Array of records to format as return output
     * @return array               Output array.  If there are records (even 1), every record will be an array ex: array(array('id'=>1),array('id'=>2))
     */
    protected function respond($recordsArray){

        if(!is_array($recordsArray)){
            // This is bad.  Throw a 500.  Responses should always be arrays.
            throw new HTTPException(
                "An error occured while retrieving records.",
                500,
                array(
                    'dev' => 'The records returned were malformed.',
                    'internalCode' => 'RESP1000',
                    'more' => ''
                )
            );
        }

        // No records returned, so return an empty array
        if(count($recordsArray) < 1){
            return array();
        }

        return array($recordsArray);

    }


    /**
     * Should be used to return data in the correct format at the end of
     * any controller functions which outputs data to the service layer.
     *
     * Ensure REST controllers extend this class and end all providing
     * functions with $this->provide($records);
     *
     * @param $records
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    protected function provide($records) {

        //echo \OrganicRest\Responses\Http::OK;

        // $chema = new \OrganicRest\Responses\Schemas\Rest(array());



        $chema = new \OrganicRest\Responses\Schemas\Rest(array(
            'status'    => \OrganicRest\Responses\Http::OK,
            'href'      => $this->getCurrent(),
            'first'     => $this->getFirst(),
            'previous'  => $this->getPrevious(),
            'next'      => $this->getNext(),
            //    'last'      => $this->getLast($records),
            'count'     => count($records),
            'limit'     => $this->limit,
            'offset'    => $this->offset,
            'sort'      => $this->sort,
            'direction' => $this->direction,
            'search'    => $this->searchFields,
            'fields'    => $this->partialFields,
            'auth'      => $this->expired,
            'records'   => $records
        ));

        return $chema;
    }

    /**
     * Get URL for previous resource / list navigation
     * TODO: Handel single item cases
     *
     * @return bool|string
     */
    protected function getFirst()
    {
        if($this->offset > 0){

            $state = $this->getState();
            $state['offset'] = 0;
            $state = urldecode(http_build_query($state));

            $route = $this->di->get('router')->getMatchedRoute();

            return $this->baseUrl().$route->getPattern() . '?' .$state;

        }

        return false;
    }

    /**
     * Get URL for previous resource / list navigation
     * TODO: Handel single item cases
     *
     * @return bool|string
     */
    protected function getPrevious()
    {
        if($this->offset > 0){

            $state = $this->getState();
            $state['offset'] -= 1;
            $state = urldecode(http_build_query($state));

            $route = $this->di->get('router')->getMatchedRoute();

            return $this->baseUrl().$route->getPattern() . '?' .$state;

        }

        return false;
    }

    /**
     * Gets Current URL for current resource / list navigation
     * TODO: Handel single item cases
     *
     * @return bool|string
     */
    protected function getCurrent()
    {
        $state = urldecode(http_build_query($this->getState()));

        $route = $this->di->get('router')->getMatchedRoute();
        $uri = $route->getPattern() . '?' .$state;

        return $this->baseUrl().$uri;
    }

    /**
     * Gets Current URL for next resource / list navigation
     * TODO: Handel single item cases
     *
     * @return bool|string
     */
    protected function getNext()
    {
        $state = $this->getState();
        $state['offset'] += 1;
        $state = urldecode(http_build_query($state));

        $route = $this->di->get('router')->getMatchedRoute();
        $uri = $route->getPattern() . '?' .$state;

        return $this->baseUrl().$uri;
    }

    /**
     * Gets Current URL for next resource / list navigation
     * TODO: Handel single item cases
     *
     * @return bool|string
     */
    protected function getLast($records)
    {
        $state = $this->getState();
        $state['offset'] = count($records) - 1; //TODO: Need to implement getTotal method before this can be added
        $state = urldecode(http_build_query($state));

        $route = $this->di->get('router')->getMatchedRoute();
        $uri = $route->getPattern() . '?' .$state;

        return $this->baseUrl().$uri;
    }

    /**
     * Returns application state data pertinent to
     * the current request
     *
     * @return array
     */
    protected function getState() {

        $search = '';
        $last_field = @end(array_keys($this->searchFields));
        if($this->searchFields) {
            foreach($this->searchFields as $field => $value){
                $search .= $field . ':' . $value;
                if($field !== $last_field) $search .= ',';
            }

        }

        $partials = '';
        $last_field = @end(array_values($this->partialFields));
        if($this->partialFields) {
            foreach($this->partialFields as $field){
                $partials .= $field;
                if($field !== $last_field) $partials .= ',';
            }

        }

        $state = array();

        $state['limit']       = $this->limit;
        $state['offset']      = $this->offset;
        $state['offset']      = $this->offset;
        $state['sort']        = $this->sort;
        $state['direction']   = $this->direction;
        $state['fields']      = $partials;
        $state['search']      = '('.$search.')';

        return $state;
    }

    /**
     * Gets base URL for the request {http(s)}://{domain}/
     *
     * @return bool|string
     */
    public function baseUrl()
    {
        return sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['HTTP_HOST']
        );
    }


    protected function _listHeaders()
    {
        return 'authorization, accept, Accept-CH, Accept-Charset, Accept-Datetime, Accept-Encoding, Accept-Ext, Accept-Features, Accept-Language, Accept-Params, Accept-Ranges, Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Access-Control-Allow-Methods, Access-Control-Allow-Origin, Access-Control-Expose-Headers, Access-Control-Max-Age, Access-Control-Request-Headers, Access-Control-Request-Method, Age, Allow, Alternates, Authentication-Info, C-Ext, C-Man, C-Opt, C-PEP, C-PEP-Info, CONNECT, Cache-Control, Compliance, Connection, Content-Base, Content-Disposition, Content-Encoding, Content-ID, Content-Language, Content-Length, Content-Location, Content-MD5, Content-Range, Content-Script-Type, Content-Security-Policy, Content-Style-Type, Content-Transfer-Encoding, Content-Type, Content-Version, Cookie, Cost, DAV, DELETE, DNT, DPR, Date, Default-Style, Delta-Base, Depth, Derived-From, Destination, Differential-ID, Digest, ETag, Expect, Expires, Ext, From, GET, GetProfile, HEAD, HTTP-date, Host, IM, If, If-Match, If-Modified-Since, If-None-Match, If-Range, If-Unmodified-Since, Keep-Alive, Label, Last-Event-ID, Last-Modified, Link, Location, Lock-Token, MIME-Version, Man, Max-Forwards, Media-Range, Message-ID, Meter, Negotiate, Non-Compliance, OPTION, OPTIONS, OWS, Opt, Optional, Ordering-Type, Origin, Overwrite, P3P, PEP, PICS-Label, POST, PUT, Pep-Info, Permanent, Position, Pragma, ProfileObject, Protocol, Protocol-Query, Protocol-Request, Proxy-Authenticate, Proxy-Authentication-Info, Proxy-Authorization, Proxy-Features, Proxy-Instruction, Public, RWS, Range, Referer, Refresh, Resolution-Hint, Resolver-Location, Retry-After, Safe, Sec-Websocket-Extensions, Sec-Websocket-Key, Sec-Websocket-Origin, Sec-Websocket-Protocol, Sec-Websocket-Version, Security-Scheme, Server, Set-Cookie, Set-Cookie2, SetProfile, SoapAction, Status, Status-URI, Strict-Transport-Security, SubOK, Subst, Surrogate-Capability, Surrogate-Control, TCN, TE, TRACE, Timeout, Title, Trailer, Transfer-Encoding, UA-Color, UA-Media, UA-Pixels, UA-Resolution, UA-Windowpixels, URI, Upgrade, User-Agent, Variant-Vary, Vary, Version, Via, Viewport-Width, WWW-Authenticate, Want-Digest, Warning, Width, X-Content-Duration, X-Content-Security-Policy, X-Content-Type-Options, X-CustomHeader, X-DNSPrefetch-Control, X-Forwarded-For, X-Forwarded-Port, X-Forwarded-Proto, X-Frame-Options, X-Modified, X-OTHER, X-PING, X-PINGOTHER, X-Powered-By, X-Requested-With, Forwarded, Scheme, Csp, Via, Autorizacaoteste, Z-Key';
    }


}

