<?php

return call_user_func(function(){

//    if(isset(explode('/',$_SERVER['REQUEST_URI'])[2]) AND explode('/',$_SERVER['REQUEST_URI'])[2] == 'views') {
//
//        include(__DIR__.'/views/index.php');
//        return;
//    }

    $nfeCollection = new \Phalcon\Mvc\Micro\Collection();

    $arr = explode('/',$_SERVER['REQUEST_URI']);

    $nfeCollection
        // VERSION NUMBER SHOULD BE FIRST URL PARAMETER, ALWAYS
        ->setPrefix('/nfe')
        // Must be a string in order to support lazy loading
        ->setHandler('\EmissorNfe\Controllers\Nfe4Controller')
        ->setLazy(true);

    // Set Access-Control-Allow headers.
    $nfeCollection->options('/', 'optionsBase');
    $nfeCollection->options('/{id:[0-9]+}', 'optionsBase');
    $nfeCollection->options('/{str}', 'optionsBase');
    $nfeCollection->options('/criar-nfe','optionsBase');
    $nfeCollection->options('/danfe', 'optionsBase');
    $nfeCollection->options('/cancelar-nfe/{id:[0-9]+}', 'optionsBase');
    $nfeCollection->options('/inutilizar-nfe', 'optionsBase');
    $nfeCollection->options('/email', 'optionsBase');
    $nfeCollection->options('/atualizar-dados-nfe-empresa/{id:[0-9]+}', 'optionsBase');
    $nfeCollection->options('/atualizar-notas-pendentes-envio', 'optionsBase');
    $nfeCollection->options('/get-xml-entrada', 'optionsBase');
    $nfeCollection->options('/get-xml-cce', 'optionsBase');
    $nfeCollection->options('/get-xml-aprovado', 'optionsBase');
    $nfeCollection->options('/get-notas-inutilizar-a3', 'optionsBase');
    $nfeCollection->options('/enviar-xml-assinado-a3', 'optionsBase');
    $nfeCollection->options('/get-notas-nao-assinadas-a3', 'optionsBase');
    $nfeCollection->options('/set-retorno-a3', 'optionsBase');
    $nfeCollection->options('/atualizar-status-nfe', 'optionsBase');
    $nfeCollection->options('/atualizar-status-nfe/{str}', 'optionsBase');
    $nfeCollection->options('/get-notas-cce-a3', 'optionsBase');
    $nfeCollection->options('/cce', 'optionsBase');
    $nfeCollection->options('/dfe', 'optionsBase');
    $nfeCollection->options('/gerar-danfe-avulso', 'optionsBase');
    $nfeCollection->options('/get-notas-consultar-a3', 'optionsBase');
    $nfeCollection->options('/gera-preview-danfe', 'optionsBase');
    $nfeCollection->options('/preview-danfe', 'optionsBase');
    $nfeCollection->options('/alocar-xml', 'optionsBase');
    $nfeCollection->options('/data-certificado', 'optionsBase');
    $nfeCollection->options('/xml/{str}', 'optionsBase');
    $nfeCollection->options('/dfe/{id:[0-9]+}', 'optionsBase');

    $nfeCollection->get('/','consultarNfe');
    $nfeCollection->get('/danfe/{str}','danfe');
    $nfeCollection->get('/xml/{str}','downloadXml');

    $nfeCollection->get('/uuu','uuu');
    $nfeCollection->get('/dfe', 'consultarDFe');
    $nfeCollection->get('/dfe/{id:[0-9]+}', 'downloadXmlTerceiro');

    $nfeCollection->post('/','criarNfe');
    $nfeCollection->post('/email','enviarEmailNFe');
    $nfeCollection->post('/inutilizar-nfe','inutilizarNfe');
    $nfeCollection->post('/cce','criarCce');
    $nfeCollection->post('/dfe', 'manifestaLote');

    $nfeCollection->put('/','cancelarNfe');

    return $nfeCollection;

});