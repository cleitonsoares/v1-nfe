<?php

namespace EmissorNfe\Helpers;

class Log
{

    function __construct()
    {

    }

    /**
     *
     * @param string $string
     * @return string
     */
    static public function setLog($string)
    {

        $fp = fopen('/var/www/logs/log-nfe', 'a+');
        $string = (!is_array($string))?$string:print_r($string,true);
        if(!fwrite($fp, date('d-m-Y H:i:s').$string."\r\n")) {
            $ferror = fopen('/var/www/erroSetLog.txt', 'a+');
            fwrite($ferror, '$data sendRequest: '. $_SERVER['REQUEST_URI']."\r\n"."\r\n");
            fclose($ferror);
        };
        fclose($fp);
    }

    static public function salvarLog($params = [])
    {

        $desc = '';
        if(is_array($params)) {
            foreach ($params as $key => $value)
            {
                if($key == 'desc') {
                    $desc = utf8_encode($value);
                }
            }
        } else {
            $desc = $params;
        }

        self::setLog($desc);
    }
}
