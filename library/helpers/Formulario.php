<?php

namespace EmissorNfe\Helpers;
use Phalcon\Mvc\View;


class Formulario
{

    function __construct()
    {

    }
	/** método usado para montar os menus dropdown
	 * @param $arrCampos (posição 0 é a chave primaria. posicao 1 é o nome do campo de descrição
	 * @return $str 
	 * @author Jose Guilherme
	 * */
	public static function montaDropDown($arrDados = array(), $nomeTabela = '', $arrCampos = array(), $idSelected='', $optionVazio=false)
	{
		$str = (($optionVazio)?'<option value=""></option>':'');
		if(count($arrDados) > 0) {
			
			foreach ($arrDados as $registro) {
		        $nomeCampoCodigo = (empty($arrCampos[0])?'cod_'.$nomeTabela:$arrCampos[0]);
		        $nomeCampoNome = (empty($arrCampos[1])?'nome_'.$nomeTabela:$arrCampos[1]);
		        $selected = ((!empty($idSelected) AND $registro[$nomeCampoCodigo] == $idSelected) ?' selected':'');
		        
		        $str .= '<option value="'.$registro[$nomeCampoCodigo].'"' . $selected;
		        $str .= '>'.$registro[$nomeCampoNome]. '</option>';
		       // echo 'ae:' . $str;
		   	}
			
		}
			
		return $str;
		
	}
	
	/** método usado para montar os menus dropdown
	 * @return $str
	 * @author Jose Guilherme
	 * */
	public function montaDropDownUni($arrDados = array(), $idSelected='')
	{
		$str = '';
			
		if(count($arrDados) > 0) {
				
			foreach ($arrDados as $registro) {
				$selected = ((!empty($idSelected) AND $registro == $idSelected) ?' selected':'');
	
				$str .= '<option value="'.$registro.'"' . $selected;
				$str .= '>'.$registro. '</option>';
			}
				
		}
			
		return $str;
	
	}
	
	
	/** método usado para montar os menus dropdown
	 * @param $arrCampos (posição 0 é a chave primaria. posicao 1 é o nome do campo de descrição
	 * @return $str
	 * @author Jose Guilherme
	 * */
	public function montaDropDownMultiplo($arrDados = array(), $nomeTabela = '', $arrCampos = array(), $arrSelected=array())
	{
		$str = '';

		
		if(count($arrDados) > 0) {
				
			foreach ($arrDados as $registro) {
				$nomeCampoCodigo = (empty($arrCampos[0])?'cod_'.$nomeTabela:$arrCampos[0]);
				$nomeCampoNome = (empty($arrCampos[1])?'nome_'.$nomeTabela:$arrCampos[1]);
				$selected = (((count($arrSelected) > 0)AND in_array($registro[$nomeCampoCodigo],$arrSelected)) ?' selected':'');
				$str .= '<option value="'.$registro[$nomeCampoCodigo].'"' . $selected;
				$str .= '>'.$registro[$nomeCampoNome]. '</option>';
				// echo 'ae:' . $str;
			}
			//die($str);
				
		}
			
		return $str;
	
	}
	
	/** método usado para montar tabelas
	 * @param $arrCampos (posição 0 é a chave primaria. posicao 1 é o nome do campo de descrição
	 		* @return $str
	 		* @author Jose Guilherme
	 		* */
	public function montaTabela($arrDados = array(), $nomeTabela = '', $arrCampos = array())
	{
		$str = '';
	
		foreach ($arrDados as $registro) {
				
		    $str .= '<tr id="'.$nomeTabela.'">';
		    foreach($arrCampos as $campo)
		    {
		        $str .= '<td>'.$registro[$campo].'</td>';
		    }
		    $str .= '</tr>';
		    
		}
		return $str;
	
	}
	
	
	/** método usado para montar os checkboxes
	 * @param $arrCampos (posição 0 é a chave primaria. posicao 1 é o nome do campo de descrição
	 * @return $str
	 * @author Jose Guilherme
	 * */
	public function montaCheckbox($arrDados = array(), $nomeTabela = '', $arrCampos = array(), $arrSelected=array())
	{
		$str = '';
	
// 		echo '<pre>';
// 		print_r($arrSelected);
// 		echo '</pre>';
		
		if(count($arrDados) > 0) {
			foreach ($arrDados as $k => $registro) {
				
				$nomeCampoCodigo = (empty($arrCampos[0])?'cod_'.$nomeTabela:$arrCampos[0]);
				$nomeCampoNome = (empty($arrCampos[1])?'nome_'.$nomeTabela:$arrCampos[1]);
				$selected = (in_array($registro[$nomeCampoCodigo],$arrSelected) ?' checked="true"':'');
	
				$str .= '<label class="checkbox ">';
				$str .= '<span><input type="checkbox" name="'.$nomeCampoCodigo.'[]" id="'.$nomeCampoCodigo.'_'.$k.'" value="'.$registro[$nomeCampoCodigo].'"' . $selected;
				$str .= '></span>'.$registro[$nomeCampoNome];
				$str .= '</label>';
			}
				
		}

		return $str;
		
	}
	
	/**
	 * Upload de imagem
	 */
	
	static public function upload_imagem($field, $dir, $wGrande = null, $hGrande = null, $wPequena = null, $hPequena = null)
	{

        //print_r($field); die;

        if($field['size'] >= 2000000) {

            return false;
        }

        $wGrande = ($wGrande)?$wGrande:500;
        $hGrande = ($hGrande)?$hGrande:500;

        $wPequena = ($wPequena)?$wPequena:300;
        $hPequena = ($hPequena)?$hPequena:300;

		$arquivo = ($field['tmp_name']) ? $field : false;
		if($arquivo){

			if(preg_match("/\.(gif|bmp|png|jpg|jpeg|JPG|JPEG|PNG|BMP|GIF){1}$/i", $arquivo['name'], $ext))  {
				$imagem_nome = 'img-'.md5(uniqid(time())) . "." . $ext[1];

//                mkdir('/var/midia/teste', 0777);

                if (!file_exists($dir)) {
                    Log::setLog('criando pasta: '.$dir);
                    mkdir($dir, 0777, true);
                    chmod($dir, 0777);
                }

                /********** COVERTE UMA IMAGEM PNG OU GIF PARA JPG QUANDO FOR LOGOMARCA DA EMPRESA *********/
                if ($field['is_logo']) {
                    self::convertImage($arquivo, $dir, $imagem_nome, 100);
                }

                Log::setLog('vai criar : '.$arquivo['tmp_name'], $dir.'/'.$imagem_nome);
                move_uploaded_file($arquivo['tmp_name'], $dir.'/'.$imagem_nome);

                $img = new \Imagick( $dir.'/'.$imagem_nome );

                /********** SALVA IMAGEM MEDIA *******/
                $img->scaleImage($wGrande, $hGrande, true);
                $img->writeImage( $dir.'/'.'thumb-'.$imagem_nome);

                /********** SALVA IMAGEM PEQUENA *******/
                $img->scaleImage($wPequena, $hPequena, true);
                $img->writeImage( $dir.'/'.'thumb2-'.$imagem_nome);

			} else {
                Log::setLog('algum erro no upload.. nao existe: '.$field['tmp_name']);
				return false;
			}
	
			return $imagem_nome;
		}
		return null;
	}


    /**
     * Converete uma imagem para o formato JPEG.
     * @param  Array   $arrFiles
     * @param  String  $diretorio
     * @param  String  $imgNome
     * @param  Integer $qualidade
     * @return Boolean
     */
    static public function convertImage($arrFiles, $diretorio, $imgNome, $qualidade)
    {
        $img = $arrFiles['tmp_name'];
        $dst = $diretorio.explode('.', $imgNome)[0];

        if (($imgInfo = getimagesize($img)) === FALSE) {
            Log::setLog('Nao foi possivel converter a imagem ('.$imgNome.') pois um erro foi encontrado!');
            return false;
        }

        $width  = $imgInfo[0];
        $height = $imgInfo[1];

        switch ($imgInfo[2])
        {
            case IMAGETYPE_GIF:
                $src = imagecreatefromgif($img);
                break;
            case IMAGETYPE_PNG:
                $src = imagecreatefrompng($img);
                break;
            default :
                Log::setLog('Nao foi necessario a conversao da imagem ('.$imgNome.') pois a extensao e aceita!');
                return false;
        }

        $tmp = imagecreatetruecolor($width, $height);

		# Remove o fundo preto da imagem:
		switch ($imgInfo[2])
		{
			case IMAGETYPE_GIF:
				$background = imagecolorallocate($tmp , 255, 255, 255);
				imagecolortransparent($tmp, $background);

			case IMAGETYPE_PNG:
				imagealphablending($tmp, false);
				imagesavealpha($tmp, true);
				break;
		}

        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tmp, $dst.'.jpg', $qualidade);

        Log::setLog('Conversao da imagem ('.$imgNome.') efetuada com sucesso! $_FILE: '.print_r($arrFiles, true));

        return true;
    }

	
	/**
	 * Upload de arquivo
	 */
	
	static public function upload_arquivo($field, $dir)
	{
		//echo 'dir: ' . $dir;
		//print_r($field);
		//die($field);
		$arquivo = ($field['tmp_name']) ? $field : false;
		if($arquivo) {
	
			if(preg_match("/\.(xls|xlsx|doc|ico|docx|ofx|txt|pdf|jpg|jpeg|JPG|JPEG|PNG|BMP|GIF|OFX){1}$/i", $arquivo["name"], $ext)) {

//				print_r($ext);
//				echo $arquivo["name"]; die;
//				die('arq-'.$arquivo['name'].rand(0,25) . '.' . $ext[1]);

				$nome = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($arquivo['name']))))));
				$nome_arq = 'arq-'.$nome.rand(0,25) . '.' . $ext[1];
                $nome_arq = str_replace(' ', '', $nome_arq);

                $url_arq = $dir .'/'. $nome_arq;

                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                    chmod($dir, 0777);
                }
				 
				move_uploaded_file($arquivo['tmp_name'], $url_arq);
				chmod($url_arq, 0777);
				 
				return $nome_arq;
			} else 	if(preg_match("/\.(pfx|PFX){1}$/i", $arquivo["name"], $ext)) {

				$file = basename($arquivo['name'], ".pfx");

				$file = str_replace(' ','',$file);
				$nome_arq = $file.rand(1,25) . '.' . $ext[1];

				$url_arq = $dir .'/'. $nome_arq;

				if (!file_exists($dir)) {
					mkdir($dir, 0777, true);
					chmod($dir, 0777);
				}

				move_uploaded_file($arquivo['tmp_name'], $url_arq);
				chmod($url_arq, 0777);

				return $nome_arq;
			}
			else {
				return 2;
			}
		}
		return 0;
	}
	
	
	
	
	
	
	/**
	 * Cria dropdown através de um array comum
	 * 
	 * @param array $array        Um array qualquer
	 * @param boolean $keyAsValue Um valor booleano
	 *
	 * @return string
	 */
	public function arrayToDopdown (array $array = null, $keyAsValue = true) 
	{
	    $str = '';
	    $val = '';
	    //print_r($array);
	    if ( is_array($array) ) {
	        do {
	            ( $keyAsValue ? $val = key($array) : $val = current($array) );
	            $str = '<option value="'.$val.'">'.current($array).'</option>'."\n";
	            next($array);
	        } while ( current($array) );
	    }
	    
	    return $str;
	    
	}//end arrayToDopdown ()
	
	/**
	 * Cria optgroups para serem usados em um menu dropdown
	 *
	 * @return string
	 */
	public function optGroup($label, $select)
	{
        $optGroup = '<optgroup label="'.$label.'" style="background-color: #1E62CF; color: #ffffff;"></optgroup><optgroup>'.
            $select.'<optgroup label="&nbsp;"></optgroup></optgroup>';
	    return $optGroup;
	}
	
	/**
	 * Completa o final de uma url com '/'
	 * 
	 * @param string $uri É a url desejada
	 * 
	 * @return string
	 */
	public function uriComplete ($uri) 
	{
	    ( !preg_match("/\/$/i",$uri) ? $uri .= '/' : null );
	    return $uri;
	    
	}//end uriComplete ()
	
	/**
     * Coloca um caminho entre '/'
     * 
     * @param string $path É o caminho que será verificado
     *
     * @return string
     */
    public function pathBetweenSlashes ($path) 
    {
        ( !preg_match("/\/$/i",$path) ? $path .= '/' : null );
        ( !preg_match("/^\//i",$path) ? $path = '/' . $path : null );
        return $path;
        
    }//end pathBetweenSlashes ()
    
    /**
     * Gera um código de uma string de vários tipos diferentes
     * As opções são "md5,halfmd5,sha1"
     * 
     * @param string $type      É o tipo de codificação permitida
     * @param string $string    É a string que será codificada
     * @param boolean $norepeat Define se a string não se repetirá
     * 
     * @return string
     */
    public function encodeAs($type, $string, $norepeat = null) 
    {
        $str = '';
        switch($type) {
            case 'md5': ( $norepeat ? 
                $str = md5($string.microtime()) : 
                $str = md5($string) ); break;
            case 'halfmd5': ( $norepeat ? 
                $str = substr(md5(md5($string.microtime())),0,16) : 
                $str = substr(md5(md5($string)),0,16)); break;
            case 'sha1': ( $norepeat ? 
                $str = sha1($string.microtime()) : 
                $str = sha1($string) ); break;
        }
        
        return $str;
        
    }
    
    public static function getDefaultMessages($comp = SUCESSO,$action = INSERIR,$msgPers = '')
    {
    	if($comp==SUCESSO) {
    		
    		$retorno['title'] = TIT_SUCCESS;
    		$retorno['error'] = false;
    		if(empty($msgPers)) {
	    		switch($action) {
	    			case INSERIR:
	    				$retorno['msg'] = MSG_CADASTRO_SUCCESS;
	    				break;
	    			case ATUALIZAR:
	    				$retorno['msg'] = MSG_ATUALIZACAO_SUCCESS;
	    				break;
	    			case EXCLUIR:
	    				$retorno['msg'] = MSG_EXCLUSAO_SUCCESS;
	    				break;
					case SELECIONAR:
						$retorno['msg'] = 'Buscar ok';
						break;
	    			default:
	    				break;
	    		}
    		} else {

                //die('fin');
    			$retorno['msg'] = $msgPers;
    		}
    	} else {
    		
    		$retorno['title'] = TIT_ERROR;
    		$retorno['error'] = true;
    		
    		if(!empty($msgPers)) {

                if (is_object($msgPers)) {

                    $objMsgs = $msgPers->getMessages();

                    foreach ($objMsgs as $msg)
                    {
                        $retorno['msg'] = $msg->getMessage();
                    }

                } else if(is_array($msgPers)) {

                    foreach ($msgPers as $msg)
                    {
                        $retorno['msg'] = $msg;
                    }
                } else {
					$retorno['msg'] = $msgPers;
				}

    		} else {
    			$retorno['msg'] = MSG_ERROR;
    		}
    	}
    	
    	return $retorno;
    }
    
    
    /** método usado para montar os menus dropdown
     * @param $arrCampos (posição 0 é a chave primaria. posicao 1 é o nome do campo de descrição
     * @return $str
     * @author Jose Guilherme
     * */
    public function montaDropDownSubAtributo($arrDados = array(), $nomeTabela = '', $arrCampos = array(), $idSelected='', $textSelecione=true)
    {
    	$str = '';
    		
    	$str .= '<option value="">-- Selecione --</option>';
    	
    	
    	
    	if(count($arrDados) > 0) {
    			
    		foreach ($arrDados as $registro) 
    		{
    			$nomeCampoCodigo = (empty($arrCampos[0])?'cod_'.$nomeTabela:$arrCampos[0]);
    			$nomeCampoNome = (empty($arrCampos[1])?'nome_'.$nomeTabela:$arrCampos[1]);
    			$selected = ((!empty($idSelected) AND $registro[$nomeCampoCodigo] == $idSelected) ?' selected':'');
    
    			$espaco = '';
    			
    			$str .= '<option value="'.$registro[$nomeCampoCodigo].'"' . $selected;
    			$str .= '>'.$registro[$nomeCampoNome]. '</option>';
    			
    		}
    		//die($str);
    			
    	}
    		
    	return $str;
    
    }

    /**
     * Enviar vários arquivos para o servidor.
     * @param $arrFiles
     * @param $dir
     * @return bool
     */
	static public function uploadMultipleFiles($arrFiles, $dir)
    {

        $file_ary = [];
        $file_count = count($arrFiles['name']);
        $file_keys = array_keys($arrFiles);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $arrFiles[$key][$i];
            }
        }

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
            chmod($dir, 0777);
        }

        foreach ($file_ary as $k => $file)
        {

            $filePath = $dir .DIRECTORY_SEPARATOR. $file['name'];

            move_uploaded_file($file['tmp_name'], $filePath);
            chmod($filePath, 0777);
        }
        
        return true;
    }
    

}