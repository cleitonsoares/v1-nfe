<?php
/**
 * Created by PhpStorm.
 * User: joze-g
 * Date: 26/05/14
 * Time: 13:01
 */

namespace EmissorNfe\Helpers;
//use Phalcon\Mvc\View;


class Formata
{

    function __construct()
    {

    }

    /**
     * Remove acentos e caracteres especiais de um caracter
     *
     * @param string $string
     * @param string $charset
     * @return string
     */

    static public function caracteres($string)
    {
        $map = array(
            '/' => '',
            'á' => 'a',
            'à' => 'a',
            'ã' => 'a',
            'â' => 'a',
            'é' => 'e',
            'ê' => 'e',
            'í' => 'i',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ú' => 'u',
            'ü' => 'u',
            'ç' => 'c',
            'Á' => 'A',
            'À' => 'A',
            'Ã' => 'A',
            'Â' => 'A',
            'É' => 'E',
            'Ê' => 'E',
            'Í' => 'I',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ú' => 'U',
            'Ü' => 'U',
            'Ç' => 'C'
        );

        return strtr($string, $map);
    }

    /**
     * Remove acentos de uma determinada string.
     *
     * @param string $string
     * @param string $charset
     * @return string
     */
    static public function removerAcentos($string, $charset = 'UTF-8')
    {
        $acentos = array(
            'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
            'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
            'C' => '/&Ccedil;/',
            'c' => '/&ccedil;/',
            'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
            'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
            'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
            'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
            'N' => '/&Ntilde;/',
            'n' => '/&ntilde;/',
            'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
            'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
            'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
            'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
            'Y' => '/&Yacute;/',
            'y' => '/&yacute;|&yuml;/',
            'a.' => '/&ordf;/',
            'o.' => '/&ordm;/'
        );

        $novo = preg_replace($acentos, array_keys($acentos), htmlentities($string, ENT_NOQUOTES, $charset));
        return strtoupper($novo);
    }


    static public function excluirAcentos($fileContents)
    {

        $texto = trim($fileContents);

        $fp = fopen('/var/www/ddd.txt', 'a+');
        fwrite($fp, '$texto1: ' . $texto);

        $aFind = array('&', 'á', 'à', 'ã', 'â', 'é', 'ê', 'í', 'ó', 'ô', 'õ', 'ú', 'ü',
            'ç', 'Á', 'À', 'Ã', 'Â', 'É', 'Ê', 'Í', 'Ó', 'Ô', 'Õ', 'Ú', 'Ü', 'Ç');
        $aSubs = array('e', 'a', 'a', 'a', 'a', 'e', 'e', 'i', 'o', 'o', 'o', 'u', 'u',
            'c', 'A', 'A', 'A', 'A', 'E', 'E', 'I', 'O', 'O', 'O', 'U', 'U', 'C');
        $novoTexto = str_replace($aFind, $aSubs, $texto);
        //$novoTexto = preg_replace("/[^a-zA-Z0-9 @,-.;:\/]/", "", $novoTexto);

        fwrite($fp, '$novoTexto: ' . $novoTexto);

        return $novoTexto;
    }

    static public function trocarCaracteresEsp($texto = '')
    {
        //$novoTexto = self::excluirAcentos($texto);

        $aFind = ['§', 'º', '*', 'ª', '|', '=', 'ƒ', '€', 'š', '"', '¶', 'ß', 'Ø', 'Þ', 'Ë', 'ï', '?', '”', 'Ø', 'õ', 'Š', '‡', '', 'µ', '~', '>', '<', '`', '¾', '\n', '\r', '<br>'];

        $aSubs = [];

        $texto = str_replace($aFind, $aSubs, $texto);

        $texto = str_replace(
            ['Ã', 'Â', 'Å', 'Á', 'Ô', 'Ò', 'â', 'Õ', 'à', 'á', 'Á', 'í', 'Í', 'é', 'É', 'ó', 'Ó', 'ú', 'Ú', 'ã', 'ô', 'Ê', 'ê', 'Ã', 'Ç', 'ç', '&', '²', '³'],
            ['A', 'A', 'A', 'A', 'O', 'O', 'a', 'O', 'a', 'a', 'A', 'i', 'I', 'e', 'E', 'o', 'O', 'u', 'U', 'a', 'o', 'E', 'e', 'A', 'C', 'c', '&amp;', '2', '3'], $texto);


        $texto = self::retornarApenasAscii($texto);
//
        if (preg_match('/[^\x20-\x7f]/', $texto)) {
            Log::setLog('tem caracteres especiaissss.. vai trocar pra ascii');
            $texto = self::retornarApenasAscii($texto);

            //Log::setLog('trocando para: '.$texto);
        }

        $texto = str_replace('&amp;amp;', '&amp;', $texto);

        return $texto;
    }

    static public function formatarDataHoraLojista($dataHora)
    {
        $arrDhs = explode(' ', $dataHora);
        if (!empty($arrDhs[1])) {


            Log::setLog('dhs: ' . $arrDhs[1]);
            if (preg_match('/:/', substr($arrDhs[1], 0, 4))) {

                $min = substr($arrDhs[1], 3, 2);
            } else {
                $min = substr($arrDhs[1], 2, 2);
            }

            Log::setLog('min: ' . $min);
            $d = substr($arrDhs[1], 0, 2) . ':' . $min . ':00';
            // $this->salvarLog('parte dat sai ' . substr($arrDhs[1],0,2) . '--' . substr($arrDhs[1],3,2));

        } else {
            $d = '00:00:00';
        }
        $datSai = Formata::toDate($arrDhs[0]) . ' ' . $d;
        //$this->salvarLog('dat sai: ' . $arrInsertVixFin['fin_dat_sai']);

        return $datSai;
    }

    /**
     * Retorna apenas caracteres ascii
     * @param $str
     * @return string
     */
    static public function retornarApenasAscii($str)
    {
        $newStr = '';
        for ($i = 0; $i != strlen($str); $i++) {
            if (ord($str[$i]) < 128) {
                $newStr .= $str[$i];
            }
        }

        return $newStr;
    }


    /**
     * Remove caracteres especiais de uma string.
     *
     * @param string $string
     * @return string
     */
    static public function escapeString($string)
    {
        $string = get_magic_quotes_gpc() ? stripslashes($string) : $string;
        $string = addcslashes($string, "\\\'\"&");
        return $string;
    }

    /**
     * Transforma valores do array em string
     * @param $arr
     * @return mixed
     */
    static public function arrayToString($arr = [])
    {
        if (count($arr) > 0) {

            foreach ($arr as $k => $reg) {
                $arr[$k] = (string)$reg;
            }
        } else {
            $arr = [];
        }

        return $arr;
    }

    static public function toDate($data = '')
    {
        list($dia, $mes, $ano) = explode('/', @$data);
        return $ano . '-' . $mes . '-' . $dia;
    }

    static public function toDateChar($data = '')
    {
        list($dia, $mes, $ano) = explode('/', @$data);
        return $ano . $mes . $dia;
    }

    static public function toDateTime($data = '')
    {
        if (!empty($data)) {
            $dataHora = explode(' ', @$data);
            $hora = '00:00:00';
            if (isset($dataHora[1])) {
                $hora = $dataHora[1];
                @$data = $dataHora[0];
            }
            list($dia, $mes, $ano) = explode('/', @$data);
            return $ano . "-" . $mes . "-" . $dia . " $hora";
        }
        return null;
    }

    static public function toMonthDay($data)
    {
        if (!empty($data)) {
            list ($dia, $mes) = explode('/', @$data);
            return $mes . $dia;
        }
        return null;
    }

    static public function toMonthDayBr($data = '')
    {
        if (!empty($data) AND $data != '0000-00-00') {

            list($ano, $mes, $dia) = explode('-', substr(@$data, 0, 10));
            return $dia . '/' . $mes;
        }
        return 0;
    }

    static public function toDateTimeUrl($data1 = '')
    {
        Log::setLog('data atual: ' . $data1);
        if (!empty($data1)) {

            $d = substr($data1, 0, 10);
            $h = substr($data1, 10, 6);

            $h1 = substr($h, 0, 2);
            $m = substr($h, 2, 2);
            $s = substr($h, 4, 2);
            Log::setLog('retorna data: ' . $d . ' ' . $h1 . ':' . $m . ':' . $s);
            return $d . ' ' . $h1 . ':' . $m . ':' . $s;
        }
        return null;
    }

    static public function toHourFromDateTime($datetime = '')
    {
        list($data, $hora) = explode(' ', $datetime);
        return self::toHourBr($hora);
    }


    static public function toDateBr($data = '')
    {
        if (!empty($data) AND $data != '0000-00-00') {

            list($ano, $mes, $dia) = explode('-', substr(@$data, 0, 10));
            return $dia . '/' . $mes . '/' . $ano;
        }
        return 0;

    }

    static public function toHourBr($hora = '')
    {
        list($a, $b) = explode(':', $hora);
        return $a . ':' . $b;

    }

    static public function toDateBrHr($data = '')
    {
        $hora = substr(@$data, 10, 18);
        list($ano, $mes, $dia) = explode('-', substr(@$data, 0, 10));
        return $dia . '/' . $mes . '/' . $ano . $hora;
    }

    static public function retornarDiaSemana($data)
    {
        $w = date('w', strtotime($data));

        $arrSemana = self::listarDiaSemana();

        return $arrSemana[$w];
    }

    static public function listarDiaSemana()
    {
        return [
            0 => 'Domingo',
            1 => 'Segunda-Feira',
            2 => 'Terça-Feira',
            3 => 'Quarta-Feira',
            4 => 'Quinta-Feira',
            5 => 'Sexta-Feira',
            6 => 'Sábado'
        ];
    }

    static public function toDateTimeConcatHour($data = '', $hour = '00:00:00')
    {
        if (!empty($data)) {
            list($dia, $mes, $ano) = explode('/', @$data);
            return $ano . '-' . $mes . '-' . $dia . ' ' . $hour;
        }
        return null;
    }

    static public function toReais($valor = '')
    {
        return number_format($valor, 2, ',', '.');
    }

    static public function toBdFloat($valor = '')
    {
        $valor = str_replace(array('R$', '.'), array('', ''), $valor);
        return str_replace(',', '.', $valor);
    }

    static public function toBdFloatPsico($valor, $lingua = '')
    {
        $valor = str_replace(array('R$', '€', ' '), array('', '', ''), $valor);

        if (empty($lingua) OR $lingua == 1) {
            $valor = str_replace(array('.'), array(''), $valor);

            return str_replace(',', '.', $valor);

        } else {
            return $valor;
        }
    }

    static public function toFloatCieloInverso($valor)
    {
        $pos = strlen($valor) - 2;

        $antes = substr($valor, 0, $pos);

        $depois = substr($valor, $pos);

        return $antes . '.' . $depois;
    }

    static public function toFloatIuguInverso($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        $valor = str_replace('R$', '', $valor);
        $valor = str_replace(' ', '', $valor);

        return $valor;
    }

    static public function toFloatPontoNota($valor = '', $tip = 1, $depoisVirgula = 2)
    {
        if ($valor == 0) {

            if ($tip == 1) {

                return '';
            } else if ($tip == 2) {
                return '0.00';
            }
        }

        return str_replace(',', '', number_format($valor, $depoisVirgula));
    }

    static public function toSemZeroDireita($valor, $qt = 2)
    {
        $valor = (string)$valor;
        $valor = rtrim($valor, "0");

        list($n1, $n2) = explode('.', $valor);

        if (empty($n2)) {
            return str_replace(',', '', number_format($valor, $qt));
        }

        return $valor;
    }

    /**
     * Trunca strings em determinado numero de caracteres
     *
     */
    static public function truncate($string, $del)
    {

        $string = strip_tags($string);
        $len = strlen($string);
        if ($len > $del) {

            $new = mb_substr($string, 0, $del, "UTF-8") . "...";
            return $new;
        }
        return $string;

    }

    /**
     * Soma valores de um array passando a chave
     * @param array
     * @param string
     * @return string
     */
    static public function obtemSomaPorChave($arr = [], $key = '')
    {
        $soma = 0;

        if (count($arr) > 0 AND !empty($key)) {

            foreach ($arr as $reg) {
                if (!empty($reg[$key])) {
                    $soma = $soma + $reg[$key];
                }
            }
        }

        return $soma;
    }

    /**
     * Trata segurança de sql injection
     * @param array $arr
     * @return array
     */
    static public function tratarSeguranca($arr = [])
    {
        if ($arr > 0) {
            //$arr = $this->array_map_r('mysql_real_escape_string', $arr);
            $arr = Formata::array_map_r('trim', $arr);
        }

        return $arr;

    }

    /**
     * array map para tratar arrays multidimensionsais
     */
    static function array_map_r($func, $arr)
    {
        $newArr = [];

        foreach ($arr as $key => $value) {
            $newArr[$key] = (is_array($value) ? Formata::array_map_r($func, $value) : (is_array($func) ? call_user_func_array($func, $value) : $func($value)));
        }

        return $newArr;
    }


    /**
     * Transforma o array em multidimensional em unidimensional passando a chave
     */
    static public function transformarArray2Para1($arr = [], $key = '')
    {
        $newArr = [];
        if (is_array($arr) AND count($arr) > 0) {
            foreach ($arr as $v) {
                if (isset($v[$key])) {
                    $newArr[] = $v[$key];
                }

            }
        }

        return $newArr;
    }


    /**
     * Translates a camel case string into a string with underscores (e.g. firstName -> first_name)
     * @param    string $str String in camel case format
     * @return    string            $str Translated into underscore format
     */
    static function fromCamelCase($str)
    {
        $str[0] = strtolower($str[0]);
        $func = create_function('$c', 'return "_" . strtolower($c[1]);');
        return preg_replace_callback('/([A-Z])/', $func, $str);
    }

    /**
     * Translates a string with hifen into camel case (e.g. first_name -> firstName)
     * @param    string $str String in hifen format
     * @param    bool $capitalise_first_char If true, capitalise the first char in $str
     * @return   string                              $str translated into camel caps
     */
    static function toCamelCase($str, $capitalise_first_char = false)
    {
        if ($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/-([a-z])/', $func, $str);
    }


    static function retornarUltimoDiaMes($mes = '', $ano = '')
    {
        return date("t", mktime(0, 0, 0, $mes, '01', $ano));
    }


    /**
     *
     * Retorna o intervalo de datas tirando os fds e feriados(no array)
     *
     * @param unknown_type $inicio
     * @param unknown_type $fim
     * @param unknown_type $arrExcluirDatas
     * @return array
     */
    static function retornarIntervaloDatasPorMesSemFDS($inicio = '', $fim = '', $arrExcluirDatas = array())
    {
        $start = new \DateTime($inicio);
        $end = new \DateTime($fim);
        $end->modify('+1 day');
        $intrDate = '1D';
        $interval = new \DateInterval('P' . $intrDate);
        $period = new \DatePeriod($start, $interval, $end);

        $days = array();

        foreach ($period as $day) {
            $dayOfWeek = $day->format('N');

            if ($dayOfWeek < 6) {

                $format = $day->format('Y-m-d');
                if (false === in_array($format, $arrExcluirDatas)) {

                    $days[] = $day->format('d/m/Y');
                }
            }


        }
        return $days;
    }

    /**
     * Retorna o intervalo de datas tirando os fds e feriados(no array)
     * @param unknown_type $inicio
     * @param unknown_type $fim
     * @param unknown_type $arrExcluirDatas
     * @return array
     */
    static function retornarIntervaloDatas($inicio = '', $ocorrencias = 1, $periodicidade = 7, $tipoFim, $repeteACada, $dataFim)
    {
        Log::setLog('parametros: ' . $inicio . ' - ' . $ocorrencias . ' - ' . $periodicidade . ' - ' . $tipoFim . ' - ' . $repeteACada . ' - ' . $dataFim);
        $start = new \DateTime($inicio);

        switch ($periodicidade) {
            case PERIOD_DIARIO:
                $t = 'D';
                break;
            case PERIOD_SEMANAL:
                $t = 'W';
                break;
            case PERIOD_MENSAL:
                $t = 'M';
                break;
            case PERIOD_ANUAL:
                $t = 'Y';
                break;
            default:
                $t = 'D';
                break;
        }


        $fimRepet = 1;

        //tipoFim = 1 (ocorrencia), tipoFim = 2 (dataFim)
        if ($tipoFim == 1) {

            $fimRepet = $ocorrencias - 1;

        } else if ($tipoFim == 2) {

            $fimRepet = new \DateTime($dataFim);
            $fimRepet = $fimRepet->modify('+1 day'); //pra ele pegar tambem o dia final
        }


        $intrDate = $repeteACada . $t;

        Log::setLog('$intrDate: ' . $intrDate);
        $interval = new \DateInterval('P' . $intrDate);


        $period = new \DatePeriod($start, $interval, $fimRepet);

        Log::setLog('$period: ' . print_r($period, true));

        $days = array();

        foreach ($period as $day) {
            $days[] = $day->format('d/m/Y');
        }

        return $days;
    }

    /**
     * Retorna o intervalo de datas tirando os fds e feriados(no array)
     * @param unknown_type $inicio
     * @param unknown_type $fim
     * @param unknown_type $arrExcluirDatas
     * @return array
     */
    static function retornarIntervaloDatasPsico($inicio = '', $ocorrencias = 1, $periodicidade = 7, $arrExcluirDatas = array())
    {

        $start = new \DateTime($inicio);
        //$s = $start->getTimestamp();

        if ($periodicidade == 15) {
            $periodicidade = 14;
        }

        //die($ocorrencias);
        $intrDate = $periodicidade . 'D';
        $interval = new \DateInterval('P' . $intrDate);
        $period = new \DatePeriod($start, $interval, $ocorrencias - 1);

        $days = array();

        // print_r($period); die;

        foreach ($period as $day) {
            $days[] = $day->format('d/m/Y');
        }

        return $days;
    }


    /**
     * Retorna o intervalo de datas tirando os fds e feriados(no array)
     * @param unknown_type $inicio
     * @param unknown_type $fim
     * @param unknown_type $arrExcluirDatas
     * @return array
     */
    static function retornarIntervaloDatasPorMes($inicio = '', $fim = '', $arrExcluirDatas = array())
    {
        $start = new DateTime($inicio);
        $end = new DateTime($fim);
        $end->modify('+1 day');
        $intrDate = '1D';
        $interval = new DateInterval('P' . $intrDate);
        $period = new DatePeriod($start, $interval, $end);

        $days = array();

        foreach ($period as $day) {
            $dayOfWeek = $day->format('N');

            if ($dayOfWeek < 8) {

                $format = $day->format('Y-m-d');
                if (false === in_array($format, $arrExcluirDatas)) {

                    $days[] = $day->format('d/m/Y');
                }
            }


        }
        return $days;
    }

    /**
     */
    static function retornarDiferencaIntervalo($inicio = '', $fim = '', $sinal = false)
    {

        $datetime1 = new \DateTime($inicio);
        $datetime2 = new \DateTime($fim);
        $interval = $datetime1->diff($datetime2);
//        print_r($interval);
        $strSinal = ($sinal) ? '%R' : '';
//        echo $interval->format($strSinal.'%a') + 1; die('feen');

        return $interval->format($strSinal . '%a') + 1;        //colocar %R pra vir o sinal
    }

    /**
     */
    static function retornarDiferencaIntervaloHoras($dataIni = '', $dataFim = '')
    {

        $t1 = strtotime($dataIni);
        $t2 = strtotime($dataFim);

        $diff = $t2 - $t1;
        return intval($diff / (60 * 60));
    }

    /**
     */
    static function ehDataAtual($inicio = '', $fim = '')
    {
        $arrI = explode(' ', $inicio);
        $arrF = explode(' ', $fim);
        $datetime1 = new \DateTime($arrI[0]);
        $datetime2 = new \DateTime($arrF[0]);
        $interval = $datetime1->diff($datetime2);

        if (!$interval->format('%a')) {
            return true;
        }
        return false;
    }


    static function ehDataMenorAtual($data)
    {
        list($dia, $mes, $ano) = explode('/', @$data);

        $dtAtual = strtotime(date('d-m-Y'));
        $dtComparacao = strtotime($dia . '-' . $mes . '-' . $ano);

        if ($dtComparacao <= $dtAtual) {
            return true;
        }

        return false;
    }


    static function gerarString()
    {
        $str = "abcdefghijlmnopqrstuvxzywk0123456789";
        $cod = "";
        for ($a = 0; $a < 7; $a++) {
            $rand = rand(0, 40);
            $cod .= substr($str, $rand, 1);
        }

        return $cod;
    }

    static function getDataProximaFatura($data = '')
    {
        if (!empty($data)) {
            $strData = Formata::toDateBr($data);
            $arrData = explode('/', $strData);

            return date('Y-m-d', strtotime("+1 month " . NUM_DIAS_FREE . ' days', mktime(0, 0, 0, $arrData[1], $arrData[0], $arrData[2])));
        }
    }

    static function ehAniversario($data, $dataAtual)
    {
        if (!empty($data)) {
            list($anoA, $mesA, $diaA) = explode('-', $data);
            list($anoB, $mesB, $diaB) = explode('-', $dataAtual);

            if ($mesA == $mesB AND $diaA == $diaB) {
                return true;
            }
        }


        return false;

    }

    static function getDataAnteriorFatura($data = '')
    {
        if (!empty($data)) {
            $strData = Formata::toDateBr($data);
            $arrData = explode('/', $strData);

            return date('Y-m-d', strtotime("+1 month " . NUM_DIAS_FREE . ' days', mktime(0, 0, 0, $arrData[1], $arrData[0], $arrData[2])));
        }
    }

    static function retornarDataExpiracaoProximaFatura($data = '', $dias = 30)
    {
        $arrData = explode('-', $data);
        //echo 'data: ' . $data;
        return date('Y-m-d', strtotime("+" . $dias . " days", mktime(0, 0, 0, $arrData[1], $arrData[2], $arrData[0])));

    }

    static function getDataParametro($data = '', $parametro = '')
    {
        $arrData = explode('-', $data);
        return date('Y-m-d', strtotime("$parametro", mktime(0, 0, 0, $arrData[1], $arrData[2], $arrData[0])));
    }

    static function retornarIdade($data_nascimento, $data_calcula)
    {
        $data_nascimento = strtotime($data_nascimento . " 00:00:00");
        $data_calcula = strtotime($data_calcula . " 00:00:00");
        $idade = floor(abs($data_calcula - $data_nascimento) / 60 / 60 / 24 / 365);

        return $idade;
    }

    static public function cript($str = '')
    {
        return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
        //return base64_encode($str);
    }

    static public function decript($str = '')
    {
        return base64_decode(str_pad(strtr($str, '-_', '+/'), strlen($str) % 4, '=', STR_PAD_RIGHT));
        //return base64_decode($str);
    }

    static public function templateEmail1($template = '', $corpo = '', $nomeUsuario = '', $emailTo = '', $idCampanha = '')
    {


        Log::setLog('iddddddd:' . $idCampanha . '-' . $emailTo . '-' . $nomeUsuario);
        $novoCorpo = str_replace(array('{ID_CAMPANHA}', '{EMAIL_TO}', '{MSG_CORPO}', '{NOME_USUARIO}'), array($idCampanha, $emailTo, $corpo, $nomeUsuario), $template);

        return $novoCorpo;
    }

    static public function templateEmail2($template = '', $corpo = '', $nomeUsuario = '', $emailTo = '', $temOla = false)
    {

        $olaUsuario = '';
        if ($temOla) {

            $olaUsuario = '<p align="left" style="padding-left:20px;font-size:18px;line-height:24px;color:#2d84b3;font-weight:bold;margin-top:0px;margin-bottom:18px;">Olá ' . $nomeUsuario . ',</p>';
        }

        Log::setLog('iddddddd:' . '-' . $emailTo . '-' . $nomeUsuario);

        $corpo = $olaUsuario . $corpo;
        $novoCorpo = str_replace(array('{EMAIL_TO}', '{MSG_CORPO}', '{NOME_USUARIO}'), array($emailTo, $corpo, $nomeUsuario), $template);

        return $novoCorpo;
    }

    static public function replaceInfoEmpresa($arrReplace = [])
    {

        $corFonte = (hexdec($arrReplace['template']['tem_cor2_padrao']) > 0xffffff / 2) ? '#000000' : '#FFFFFF'; //define o contraste de uma cor

        $novoCorpo = str_replace([
            '{ENDERECO_EMPRESA}',
            '{BAIRRO_EMPRESA}',
            '{NRO_EMPRESA}',
            '{COMPLEMENTO_EMPRESA}',
            '{CIDADE_UF_EMPRESA}',
            '{TELEFONE_EMPRESA}',
            '{COR_EMPRESA}',
            '{COR_EMPRESA2}',
            '{COR_TEXTO}',
            '{LINK_SITE}',
            '{LOGO_EMPRESA}',

        ], [
            @$arrReplace['emp_endereco'],
            @$arrReplace['emp_bairro'],
            @$arrReplace['emp_nro'],
            @$arrReplace['emp_complemento'],
            @$arrReplace['emp_cidade'] . ' - ' . @$arrReplace['emp_uf'],
            @$arrReplace['emp_fone'],
            @$arrReplace['template']['tem_cor1_padrao'],
            @$arrReplace['template']['tem_cor2_padrao'],
            $corFonte,
            @$arrReplace['emp_dominio'],
            CACHE_IMG . @$arrReplace['logomarca'],

        ], @$arrReplace['bodytemplate']);

        return $novoCorpo;
    }

    /**
     * Filtra o array retornando apenas os valores com as chaves do segundo parametro
     * @param array $arrFiltro
     * @param array $arrK
     * @return array
     */
    static public function getParametroFiltro($arrFiltro = array(), $arrK = array())
    {
        $arrFiltroNew = array();
        if (count($arrFiltro) > 0) {
            foreach ($arrFiltro as $k => $reg) {
                if (!empty($reg) AND in_array($k, $arrK)) {
                    $arrFiltroNew[$k] = $reg;
                }
            }
        }

        return $arrFiltroNew;
    }

    public static function timeAgo($secs)
    {
        $bit = array(
            ' ano' => $secs / 31556926 % 12,
            ' semana' => $secs / 604800 % 52,
            ' dia' => $secs / 86400 % 7,
            ' hora' => $secs / 3600 % 24,
            ' minuto' => $secs / 60 % 60,
            ' segundo' => $secs % 60
        );

        foreach ($bit as $k => $v) {
            if ($v > 1) $ret[] = $v . $k . 's';
            if ($v == 1) $ret[] = $v . $k;
        }
        array_splice($ret, count($ret) - 1, 0, 'and');
        $ret[] = 'atrás';

        return $ret[0] . ' ' . array_pop($ret);
        //return join(' ', $ret);
    }

    public static function dataExtenso($data = '')
    {
        //die($data);
        $dataHoje = date('Y-m-d');
        list($dataA, $horaA) = explode(' ', $data);

        if ($dataA == $dataHoje) {
            return $horaA;
        } else {
            list($ano, $mes, $dia) = explode('-', $dataA);
            return $dia . ' de ' . Calendario::getMes($mes, 1, 2);
        }
    }

    public static function cor($cor = 1, $tipoDesc = 1)
    {
        $cor = ((!empty($cor)) ? $cor : 1);
        $arrCoresDesc = array();

        if ($tipoDesc == 1) {
            $arrCoresDesc = array(
                1 => 'danger-ant',
                2 => 'success-ant',
                3 => 'info-ant',
                4 => 'inverse',
                5 => 'primary',
                6 => 'warning-ant',
                7 => 'vinho',
                8 => 'rosa-ant',
                9 => 'laranja',
                10 => 'rosa-claro',
                11 => 'marrom'
            );
        } else if ($tipoDesc == 2) {
            $arrCoresDesc = array(
                1 => '#ff5f5f',
                2 => '#3fcf7f',
                3 => '#5191d1',
                4 => '#233445',
                5 => '#13c4a5',
                6 => '#f4c414',
                7 => '#660000',
                8 => '#840099',
                9 => '#FF4500',
                10 => '#FF00FF',
                11 => '#996633'
            );
        }

        return $arrCoresDesc[$cor];
    }


    public static function formatarMergeFiltro($arrFiltro = array(), $arrFiltroPadrao = array(), $conditions = '')
    {
        // echo 'cond: '.$conditions;
        // print_r($arrFiltro);
        // die('fin');

        if (count($arrFiltro) > 0) {
            foreach ($arrFiltro as $k => $filtro) {
                if (!preg_match('/:' . $k . ':/', $conditions)) {
                    unset($arrFiltro[$k]);
                }
            }

        }
        //print_r($arrFiltro); die;
        //print_r(array_merge($arrFiltro,$arrFiltroPadrao)); //die;

        if (!is_array($arrFiltro)) {
            $arrFiltro = array();
        }

        return array_merge($arrFiltro, $arrFiltroPadrao);
    }

    public static function formatarCharConcat($str = '')
    {
        if (!isset($str)) {
            return array('', '');
        }

        list($p1, $p2) = explode('#', $str);

        $p1 = ($p1 == 'null') ? '' : $p1;
        $p2 = ($p2 == 'null') ? '' : $p2;

        return array($p1, $p2);

    }

    public static function getAnoMes($dat)
    {
        $arrDat = explode(' ', $dat);
        $arrDat = explode('-', $arrDat[0]);

        return $arrDat[0] . $arrDat[1];

    }

    public static function toMongoObject($arrParam, $obj)
    {
        foreach ($arrParam as $key => $value) {
            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    if (is_array($v)) {

                        foreach ($v as $k2 => $v2) {
                            $value[$k][$k2] = utf8_encode($value[$k][$k2]);
                        }
                    } else {
                        $value[$k] = utf8_encode($value[$k]);
                    }

                }
                $obj->$key = $value;

            } else {
                $obj->$key = utf8_encode($value);
            }

        }

        return $obj;
    }

    static function object2Array($result)
    {
        $array = array();
        foreach ($result as $key => $value) {
            if (is_object($value)) {
                $array[$key] = self::object2Array($value);
            } elseif (is_array($value)) {
                $array[$key] = self::object2Array($value);
            } else {
                $array[$key] = $value;
            }
        }
        return $array;
    }

    /**
     *
     * Transforma um array em um objeto. Preparado apenas para manipulação do mongodb. Retorna o id e não apenas o _id
     * @param $obj
     * @param $arrKeys
     * @return array
     */
    public static function toArrayObject($obj, $arrKeys)
    {
        $arr = array();

        foreach ($obj as $k2 => $m) {
            foreach ($arrKeys as $chave => $valorDoArrKeys) {
                if (isset($m->$chave)) {

                    if (count($valorDoArrKeys) > 0) {

                        foreach ($m->$chave as $keyRegistroInterno => $v) {
                            foreach ($v as $k3 => $valorInterno) {
                                $arr[$k2][$chave][$keyRegistroInterno][$k3] = $m->{$chave}[$keyRegistroInterno][$k3];
                            }
                        }

                    } else {

                        $arr[$k2][$chave] = $m->$chave;
                    }
                }

                //die($m->_id);

                if ($chave == '_id') {

                    $arrId = (array)$m->_id;
                    $arr[$k2]['id'] = $arrId['$id'];

                }
            }


        }

        return $arr;
    }

    public static function uniqueMultidimArrayKey($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        if (count($array) > 0) {
            foreach ($array as $val) {
                //if(array_key_exists($key,$val[$key])) {
                if (!in_array($val[$key], $key_array)) {
                    $key_array[$i] = $val[$key];
                    $temp_array[] = $val;
                }
                $i++;
                //}

            }
        }

        return $temp_array;

    }

    public static function toArrayUtf8($arr)
    {

        $arrNovo = [];
        foreach ($arr as $k1 => $reg) {
            if (is_array($reg)) {
                foreach ($reg as $k2 => $v) {
                    if (is_array($v)) {

                        foreach ($v as $k3 => $v3) {
//                            $arrNovo[$k1][$k2][$k3] = utf8_decode($v3);

                            if (is_array($v3)) {

                                foreach ($v3 as $k4 => $v4) {

                                    if (is_array($v4)) {

                                        foreach ($v4 as $k5 => $v5) {
                                            $arrNovo[$k1][$k2][$k3][$k4][$k5] = utf8_decode($v5);

                                        }
                                    } else {

                                        $arrNovo[$k1][$k2][$k3][$k4] = utf8_decode($v4);
                                    }
                                }
                            } else {
                                $arrNovo[$k1][$k2][$k3] = utf8_decode($v3);
                            }


                        }
                    } else {
                        $arrNovo[$k1][$k2] = utf8_decode($v);
                    }

                }
            } else {
                $arrNovo[$k1] = utf8_decode($reg);
            }
        }
//        print_r($arrNovo); die;
        return $arrNovo;
    }

    public static function toArrayUtf8Enc($arr)
    {

        $arrNovo = [];
        foreach ($arr as $k1 => $reg) {
            if (is_array($reg)) {
                foreach ($reg as $k2 => $v) {
                    if (is_array($v)) {

                        foreach ($v as $k3 => $v3) {
//                            $arrNovo[$k1][$k2][$k3] = utf8_decode($v3);

                            if (is_array($v3)) {

                                foreach ($v3 as $k4 => $v4) {

                                    if (is_array($v4)) {

                                        foreach ($v4 as $k5 => $v5) {
                                            $arrNovo[$k1][$k2][$k3][$k4][$k5] = utf8_encode($v5);

                                        }
                                    } else {

                                        $arrNovo[$k1][$k2][$k3][$k4] = utf8_encode($v4);
                                    }
                                }
                            } else {
                                $arrNovo[$k1][$k2][$k3] = utf8_encode($v3);
                            }


                        }
                    } else {
                        $arrNovo[$k1][$k2] = utf8_encode($v);
                    }

                }
            } else {
                $arrNovo[$k1] = utf8_encode($reg);
            }
        }
//        print_r($arrNovo); die;
        return $arrNovo;
    }


    public static function toArrayUtf8Inverso($arr)
    {

        $arrNovo = [];
        foreach ($arr as $k1 => $reg) {
            if (is_array($reg)) {
                foreach ($reg as $k2 => $v) {
                    if (is_array($v)) {

                        foreach ($v as $k3 => $v3) {

                            if (is_array($v3)) {

                                foreach ($v3 as $k4 => $v4) {
                                    $arrNovo[$k1][$k2][$k3][$k4] = utf8_encode($v4);
                                }
                            } else {
                                $arrNovo[$k1][$k2][$k3] = utf8_encode($v3);
                            }


                        }
                    } else {
                        $arrNovo[$k1][$k2] = utf8_encode($v);
                    }

                }
            } else {
                $arrNovo[$k1] = utf8_encode($reg);
            }
        }
//        print_r($arrNovo); die;
        return $arrNovo;
    }


    /**
     * Método responsável em buscar um determinado CEP num vetor de fretes.
     * @param String $cep
     * @param Array $arrFretes
     */
    public static function filterCEP($cep, $arrFretes = [])
    {
        $arrRetorno = [];

        if (count($arrFretes) > 0) {

            # Todas as possibilidades existentes para o CEP fornecido:
            $arrQuebraCep = [
                'PARTE_1' => $cep{0},
                'PARTE_2' => substr($cep, 0, 2),
                'PARTE_3' => substr($cep, 0, 3),
                'PARTE_4' => substr($cep, 0, 4),
                'PARTE_5' => substr($cep, 0, 5),
                'PARTE_6' => substr($cep, 0, 6),
                'PARTE_7' => substr($cep, 0, 7),
                'PARTE_8' => $cep
            ];

            foreach ($arrFretes as $item) {
                $cepFrete = trim($item['frt_cep_mascara']);
                $cepTamanho = strlen($cepFrete);

                # Verifica se existe algum frete que inicie com o número do CEP fornecido:
                if ($cepTamanho === 1 AND $cepFrete === $arrQuebraCep['PARTE_1']) {
                    $arrRetorno = $item;
                    break;
                }

                # Verifica se o CEP inicia com os dois primeiros números do CEP fornecido:
                if ($cepTamanho === 2 AND $cepFrete === $arrQuebraCep['PARTE_2']) {
                    $arrRetorno = $item;
                    break;
                }

                # Verifica se o CEP inicia com os três primeiros números do CEP fornecido:
                if ($cepTamanho === 3 AND $cepFrete === $arrQuebraCep['PARTE_3']) {
                    $arrRetorno = $item;
                    break;
                }

                # Verifica se o CEP inicia com os quatros primeiros números do CEP fornecido:
                if ($cepTamanho === 4 AND $cepFrete === $arrQuebraCep['PARTE_4']) {
                    $arrRetorno = $item;
                    break;
                }

                # Verifica se o CEP inicia com os cinco primeiros números do CEP fornecido:
                if ($cepTamanho === 5 AND $cepFrete === $arrQuebraCep['PARTE_5']) {
                    $arrRetorno = $item;
                    break;
                }

                # Verifica se o CEP inicia com os seis primeiros números do CEP fornecido:
                if ($cepTamanho === 6 AND $cepFrete === $arrQuebraCep['PARTE_6']) {
                    $arrRetorno = $item;
                    break;
                }

                # Verifica se o CEP inicia com os sete primeiros números do CEP fornecido:
                if ($cepTamanho === 7 AND $cepFrete === $arrQuebraCep['PARTE_7']) {
                    $arrRetorno = $item;
                    break;
                }

                # Verifica se o CEP é idêntico ao CEP fornecido:
                if ($cepTamanho === 8 AND $cepFrete === $arrQuebraCep['PARTE_8']) {
                    $arrRetorno = $item;
                    break;
                }
            }
        }

        return $arrRetorno;
    }


    public static function sendRequest($metodo = 'GET', $url = '', $strRowBody = '', $arrHeader = array())
    {

        $response = '';
        $ch = curl_init();
        switch ($metodo) {
            case 'POST':

//                ob_start();
//                $out = fopen('php://output', 'w');

                //$url = "http://localhost/v1/v1/sistema/sit-cto";
                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
                curl_setopt($ch, CURLOPT_POSTFIELDS, $strRowBody);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                curl_setopt($ch, CURLOPT_TIMEOUT, 90);
//                curl_setopt($ch, CURLOPT_VERBOSE, true);
//                curl_setopt($ch, CURLOPT_STDERR, $out);

                $response = curl_exec($ch);
//                fclose($out);
//                $debug = ob_get_clean();
//                var_dump($debug);

                //var_dump($response);
                curl_close($ch);
                break;

            case 'GET':

                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90); # timeout after 10 seconds, you can increase it
                curl_setopt($ch, CURLOPT_URL, $url);
                $response = curl_exec($ch);
                curl_close($ch);
                break;

            case 'PUT':

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $strRowBody);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);

                $response = curl_exec($ch);

                curl_close($ch);
                break;

            case 'PATCH':
                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $strRowBody);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                curl_setopt($ch, CURLOPT_TIMEOUT, 90);
                $response = curl_exec($ch);
                curl_close($ch);
                break;

            case 'DELETE':
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $strRowBody);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);

                $response = curl_exec($ch);

                curl_close($ch);
                break;
        }

        return $response;
    }

    public static function sendRequestAuth($metodo = 'GET', $url = '', $strRowBody = '', $arrHeader = array())
    {

        $response = '';
        $ch = curl_init();
        switch ($metodo) {
            case 'POST':

//                ob_start();
//                $out = fopen('php://output', 'w');

                //$url = "http://localhost/v1/v1/sistema/sit-cto";
//                curl_setopt($ch,CURLOPT_HTTPHEADER,$arrHeader);
//                curl_setopt($ch,CURLOPT_URL,$url);
//                curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
//                curl_setopt($ch,CURLOPT_POSTFIELDS,$strRowBody);
//                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
//                curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
//                curl_setopt($ch,CURLOPT_TIMEOUT, 90);
//                curl_setopt($ch, CURLOPT_USERPWD, "4f36f0bebec9dff71446f4:46912abf411c8fc9cf");

                // $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERPWD, "4f36f0bebec9dff71446f4:46912abf411c8fc9cf");
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);
                curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
                curl_setopt($ch, CURLOPT_POSTFIELDS, $strRowBody);


                echo 'aaaaaaaaaaaaaaaaaaa';

//                $output = curl_exec($ch);
                // $info = curl_getinfo($ch);
                curl_setopt($ch, CURLOPT_VERBOSE, true);

                $response = curl_exec($ch);

                curl_close($ch);

                // curl_setopt($ch, CURLOPT_STDERR, $out);

//                fclose($out);
//                $debug = ob_get_clean();
//                var_dump($debug);

                var_dump($response);
                curl_close($ch);
                break;

            case 'GET':

                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90); # timeout after 10 seconds, you can increase it
                curl_setopt($ch, CURLOPT_URL, $url);
                $response = curl_exec($ch);
                curl_close($ch);
                break;

            case 'PUT':

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $strRowBody);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);

                $response = curl_exec($ch);

                curl_close($ch);
                break;

            case 'PATCH':
                curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeader);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $strRowBody);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                curl_setopt($ch, CURLOPT_TIMEOUT, 90);
                $response = curl_exec($ch);
                curl_close($ch);
                break;
        }

        return $response;
    }

    public static function getRespostaRequest($jsonResposta)
    {
        $arrRet = json_decode($jsonResposta, true);
        return $arrRet['records'];
    }

    /**
     * Inverte o array no formato  $arrDataSnap = array('COD_PRODUTO' => array(1,2,3), 'NOME_PRODUTO' => array('ff','aa','bb')); para array(0 => array('NOME_PRODUTO' => 'aa')
     * @param array $arrDataSnap
     * @return array
     */
    public static function inverterArrayDs($arrDataSnap = [])
    {
        $arrNewDataSnap = [];
        foreach ($arrDataSnap as $campoDataSnap => $arrInterno) {
            foreach ($arrInterno as $k => $valor) {
                if ($campoDataSnap != 'table') {
                    $arrNewDataSnap[$k][$campoDataSnap] = $valor;
                }

            }
        }

        return $arrNewDataSnap;
    }

    /**
     * Transforma o objeto em array já com os dados vindos da api (key 'records')
     * @param $obj
     * @return mixed
     */
    public static function getRecords($obj)
    {
        if ($obj) {
            $arr = json_decode($obj, true);
            return $arr['records'];
        }

        return null;

    }

    /**
     * Métod responsável em retornar o valor aceito pela base de dados referente
     * ao custo total da transação. Ex: 100 para 1.00.
     * @param  $valor
     * @return String
     */
    public static function getValorTransacaoCielo($valor)
    {
        if ($valor) {
            $tamanho = strlen($valor);

            $v1 = substr($valor, 0, $tamanho - 2);
            $v2 = substr($valor, $tamanho - 2);

            return $v1 . '.' . $v2;
        }

        return 0;
    }

    /**
     * Método responsável em transformar uma determinada data que esteja no formato
     * d/m/Y H:i:s para o formato Y-m-d H:i:s.
     * @param  $data
     * @return String
     */
    public static function getDataTransacaoCielo($data)
    {
        $timestamp = date_create_from_format('d/m/Y H:i:s', $data);
        return ($timestamp) ? date_format($timestamp, 'Y-m-d H:i:s') : date('Y-m-d H:i:s');
    }

    /**
     * Método responsável em transformar uma determinada data que esteja no formato
     * YYYYMMDD para o formato YYYY-MM-DD.
     * @param  string $data
     * @return string $strData
     */
    public static function getDataPadraoBanco($data)
    {
        $strData = '';
        if ($data) {

            $data = intval($data) ? (string)$data : $data;

            $ano = trim(substr($data, 0, 4));
            $mes = trim(substr($data, 4, 2));
            $dia = trim(substr($data, 6, 2));

            $strData = $ano . '-' . $mes . '-' . $dia;
        }
        return $strData;
    }

    /**
     * Retorna a string contendo a classe de estilo para uma determinada forma de pagamento.
     * @param  string $strFormaPgto
     * @return string
     */
    public static function getIconePgto($strFormaPgto)
    {
        if ($strFormaPgto) {
            if (preg_match('/^dinheiro/', $strFormaPgto) OR preg_match('/Dinheiro/', $strFormaPgto)) {
                return 'fa-money';
            } else if (preg_match('/^visa/', $strFormaPgto)) {
                return 'fa-cc-visa';
            } else if (preg_match('/^mastercard/', $strFormaPgto)) {
                return 'fa-cc-mastercard';
            } else if (preg_match('/^Boleto/', $strFormaPgto)) {
                return 'fa-barcode';
            } else if (preg_match('/^Cart/', $strFormaPgto)) {
                return 'fa-credit-card';
            } else if (preg_match('/^Transf/', $strFormaPgto)) {
                return 'fa-exchange';
            }
        }
        return 'fa-check';
    }

    /**
     * Método responsável em recolher a chave primária para a tabela de conciliação.
     * @param  string $sequencia
     * @param  string $dtLancamento
     * @return string $conCode
     */
    public static function getKeyConciliacao($sequencia, $dtLancamento)
    {
        $conCode = 0;
        if ($sequencia && $dtLancamento) {

            $arrData = explode('-', $dtLancamento);
            $strData = $arrData[0] . $arrData[1] . $arrData[2];

            $conCode = intval($strData . $sequencia);
        }
        return $conCode;
    }

    /**
     * Verifica se duas datas são iguais.
     * @param  string $dtOne
     * @param  string $dtTwo
     * @return bool   $retorno
     */
    public static function ehDataIgual($dtOne, $dtTwo)
    {
        $retorno = false;
        if ($dtOne && $dtTwo) {

            $dtOne = strtotime(trim(substr($dtOne, 0, 10)));
            $dtTwo = strtotime(trim(substr($dtTwo, 0, 10)));

            $retorno = ($dtOne === $dtTwo) ? true : false;
        }
        return $retorno;
    }

    /**
     * Verifica se dois valores decimais/ponto flutuante são iguais.
     * @param  string $valOne
     * @param  string $valTwo
     * @return bool   $retorno
     */
    public static function ehValorIgual($valOne, $valTwo)
    {
        $retorno = false;
        if ($valOne && $valTwo) {

            $valOne = (float)$valOne;
            $valOne = ($valOne < 0) ? ($valOne * (-1)) : $valOne;

            $valTwo = (float)$valTwo;

            $retorno = ($valOne === $valTwo) ? true : false;
        }
        return $retorno;
    }

    /**
     * Transforma a data no formato do PagSeguro para o formato da nossa base de dados.
     * @param  $data
     * @return string
     */
    public static function getDataTransacaoPagSeguro($data)
    {
        $dtCadastro = (string)$data;
        $dtCadastro = date('Y-m-d H:i:s', strtotime($dtCadastro));
        return $dtCadastro;
    }

    /**
     * Criar nome de subdominio vinculado a um nome de razao social
     */
    public static function getNomeSubDominio($nomeEmpresa)
    {
        $nomeEmpresa = strtolower($nomeEmpresa);
        $nomeEmpresa = preg_replace('/[^A-Za-z0-9\-]/', '', $nomeEmpresa);
        $nomeEmpresa = preg_replace(['/(LTDA|ME|SA)$/i'], [''], $nomeEmpresa);

        return $nomeEmpresa;
    }

    /**
     * Retorna o nome do estado através de sua sigla.
     */
    public static function getDescEstado($sigla)
    {
        $descricao = '';

        if ($sigla) {
            $arrEstados = [
                'AC' => 'Acre',
                'AL' => 'Alagoas',
                'AP' => 'Amapá',
                'AM' => 'Amazonas',
                'BA' => 'Bahia',
                'CE' => 'Ceará',
                'DF' => 'Distrito Federal',
                'ES' => 'Espírito Santo',
                'GO' => 'Goiás',
                'MA' => 'Maranhão',
                'MT' => 'Mato Grosso',
                'MS' => 'Mato Grosso do Sul',
                'MG' => 'Minas Gerais',
                'PA' => 'Pará',
                'PB' => 'Paraíba',
                'PR' => 'Paraná',
                'PE' => 'Pernambuco',
                'PI' => 'Piauí',
                'RJ' => 'Rio de Janeiro',
                'RN' => 'Rio Grande do Norte',
                'RS' => 'Rio Grande do Sul',
                'RO' => 'Rondônia',
                'RR' => 'Roraima',
                'SC' => 'Santa Catarina',
                'SP' => 'São Paulo',
                'SE' => 'Sergipe',
                'TO' => 'Tocantins'
            ];

            return $arrEstados[$sigla];
        }

        return $descricao;
    }

    /**
     * retornar meses
     */
    static public function retornarMeses($tipo = 1)
    {
        if ($tipo == 1) {
            return array('01' => 'Janeiro',
                '02' => 'Fevereiro',
                '03' => 'Março',
                '04' => 'Abril',
                '05' => 'Maio',
                '06' => 'Junho',
                '07' => 'Julho',
                '08' => 'Agosto',
                '09' => 'Setembro',
                '10' => 'Outubro',
                '11' => 'Novembro',
                '12' => 'Dezembro');
        } else {
            return array('1' => 'Janeiro',
                '2' => 'Fevereiro',
                '3' => 'Março',
                '4' => 'Abril',
                '5' => 'Maio',
                '6' => 'Junho',
                '7' => 'Julho',
                '8' => 'Agosto',
                '9' => 'Setembro',
                '10' => 'Outubro',
                '11' => 'Novembro',
                '12' => 'Dezembro');
        }
    }

    public static function preencherStr($input, $tam, $strPreencher, $lado = STR_PAD_LEFT)
    {
        return str_pad($input, $tam, $strPreencher, $lado);
    }

    public static function preencherStrTamFixo($input, $tam, $strPreencher, $lado = STR_PAD_LEFT)
    {
        $aux = str_pad($input, $tam, $strPreencher, $lado);

        return substr($aux, 0, $tam);
    }

    public static function toHoraPreencherZero($hora)
    {
        list($h, $m) = explode(':', $hora);

        $h = self::preencherStr($h, 2, '0', STR_PAD_LEFT);
        $m = self::preencherStr($m, 2, '0', STR_PAD_RIGHT);

        return $h . ':' . $m;

    }

    public static function getStatusAutorizados()
    {
        return ['100', '539', '204'];
    }

    public static function getStatusCancelados()
    {
        return ['101', '135', '151', '155', '218', '573'];
    }

    public static function getStatusInutilizados()
    {
        return ['102', '563', '206', '256'];
    }

    public static function getStatusCce()
    {
        return ['135', '573'];
    }

    public static function getStatusDenegados()
    {
        return ['110', '301', '302'];
    }

    public static function toArrayToArray($arr)
    {
        $newArr = [];
        foreach ($arr as $a) {
            //print_r($a);//die;
            $newArr[] = $a;
        }
        //print_r($newArr); die;

        return $newArr;
    }

    /**
     * Algumas mensagens personalizadas
     * @param $retSefaz
     * @return string
     */
    public static function descMsgSefazPersonalizadas($retSefaz)
    {

        if ($retSefaz['codSefaz'] == '100') {

            $msg = 'Documento autorizado';
        }
//        else if($retSefaz['codSefaz'] == '204') {
//
//            $msg = 'Duplicidade de NF-e. Esta nota j&aacute; foi autorizada.';
//
//        }
        else if ($retSefaz['codSefaz'] == '135') {

            $msg = "Documento cancelado. {$retSefaz['strMotivo']}";

        } else {

            if (preg_match('/Temporarily Unavailable/', $retSefaz['strMotivo']) OR preg_match('/Curl/', $retSefaz['strMotivo'])) {

                $msg = "Algum erro ocorreu devido a SEFAZ em modo de {$retSefaz['modo']} estar fora do ar.";

            } else if (preg_match('/Operation timed out/', $retSefaz['strMotivo']) OR preg_match('/An error occurred while trying to communication via soap/', $retSefaz['strMotivo'])) {

                $msg = "SEFAZ em modo de {$retSefaz['modo']} apresentando instabilidade, tente novamente...";

            } else if (preg_match('/Internal Server Error/', $retSefaz['strMotivo'])) {

                $msg = 'Erro no servividor da SEFAZ, tente novamente...';

            } else {

                $msg = "A Sefaz retornou com o seguinte status: {$retSefaz['codSefaz']} ({$retSefaz['strMotivo']})";
            }
        }

        return $msg;
    }

    public static function getSituacaoPorCodSefaz($codSefaz = 0)
    {

        switch ($codSefaz) {

            case 100:
            case 150:
            case 151:
            case 523: //é erro mas mantem em situaçao autorizada - Já pussui cce vinculada
            case 220: //é erro mas mantem em situaçao autorizada
                return SIT_NFE_AUTORIZADO;
            case 539:
            case 204:
                return STATUS_INTERNO_DUPLICIDADE;
            case 101:
            case 135:
            case 218:
            case 573:
            case 155:
                return SIT_NFE_CANCELADO;
            case 103:
            case 104:
            case 105:
            case 107:
                return SIT_NFE_AGUARDANDO;
            case 106:
            case 108:
            case 328:
            case 109:
                return SIT_NFE_ERRO;
            case 110:
            case 301:
            case 302:
            case 303:
                return SIT_NFE_DENEGADO;
            case 102:
            case 563:
            case 256:
            case 206:
                return SIT_NFE_INUTILIZADO;
            case STATUS_INTERNO_ERRO_VALIDACAO_INICIAL:
                return SIT_NFE_ALERTA;

            //case STATUS_INTERNO_CRIADA_NOTA_A3:
//            case STATUS_INTERNO_ERRO_NOTA_A3;
//                return SIT_NFE_PENDENTE_ASSINATURA_A3;

        }

        return SIT_NFE_ERRO;
    }

    public static function getAcaoPorCodSefaz($codSefaz = 0)
    {

        switch ($codSefaz) {

            case 100:
            case 539:
            case 204:
                return FIN_ACAO_CONCLUIDO;
            case 101:
            case 102:
            case 135:
            case 151:
            case 155:
            case 218:
            case 573:
            case 110:
            case 301:
            case 302:
            case 303:
                return FIN_ACAO_CANCELADO;
            case STATUS_INTERNO_ERRO_VALIDACAO_INICIAL:
            case STATUS_INTERNO_AGUARDANDO:
                return FIN_ACAO_FATURADO;
            default:
                return false;

        }


    }

    public static function getContentsInBrackets($strTexto)
    {
        preg_match_all("/\[[^\]]*\]/", $strTexto, $matches);
        return ($matches[0][0]) ? $matches[0][0] : '';
    }

    public static function getUrlAmigavel($string)
    {
        if ($string) {

            $newString = self::removerAcentos($string);
            $newString = strtolower($newString);
            $newString = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($newString))))));

            return str_replace(' ', '-', $newString);
        }
        return null;
    }


    /**
     * Método responsável em formatar uma STRING para o padrão CPF ou CNPJ.
     * @static
     * @param  String $valor
     * @param  String $tipo
     * @return String $formatado
     */
    public static function formataCPFeCNPJ($valor, $tipo = 'CPF')
    {
        $formatado = '';
        if ($valor) {

            $valor = trim($valor);
            if ($tipo === 'CPF') {

                $formatado = substr($valor, 0, 3) . '.';
                $formatado .= substr($valor, 3, 3) . '.';
                $formatado .= substr($valor, 6, 3) . '-';
                $formatado .= substr($valor, 9, 2) . '';

            } elseif ($tipo === 'CNPJ') {

                $formatado = substr($valor, 0, 2) . '.';
                $formatado .= substr($valor, 2, 3) . '.';
                $formatado .= substr($valor, 5, 3) . '/';
                $formatado .= substr($valor, 8, 4) . '-';
                $formatado .= substr($valor, 12, 14) . '';
            }
        }

        return $formatado;
    }


    /**
     * Método responsável em formatar uma STRING para o padrão de telefones normais.
     * @static
     * @param  String $valor
     * @return String $formatado
     */
    public static function formataTelefone($valor)
    {
        $formatado = '';
        if ($valor) {

            $formatado = '(' . substr($valor, 0, 2) . ') ';
            $formatado .= substr($valor, 2, 4) . '-';
            $formatado .= substr($valor, 6);
        }

        return $formatado;
    }


    /**
     * Método responsável em formatar uma STRING para o padrão de celulares.
     * @static
     * @param  String $valor
     * @return String $formatado
     */
    public static function formataCelular($valor)
    {
        $formatado = '';
        if ($valor) {

            $qtde = strlen(trim($valor));
            if ($qtde === 11) {

                $formatado = '(' . substr($valor, 0, 2) . ') ';
                $formatado .= substr($valor, 2, 5) . '-';
                $formatado .= substr($valor, 7);

            } else {

                $formatado = '(' . substr($valor, 0, 2) . ') ';
                $formatado .= substr($valor, 2, 4) . '-';
                $formatado .= substr($valor, 6);
            }
        }

        return $formatado;
    }


    /**
     * Método responsável em retornar uma data no padrão YYYY-MM-DD HH:II:SS para inserção no banco.
     * @param String $data
     * @return NULL || String
     */
    public static function toDateTimeFormat($data = '')
    {
        if (!empty($data)) {

            $arrData = explode('/', @$data);
            if (count($arrData) === 1) {

                return $data;

            } else {

                list($dia, $mes, $ano) = explode('/', @$data);
                return $ano . '-' . $mes . '-' . $dia . ' 00:00:00';
            }
        }

        return null;
    }


    /**
     * Método responsável em retornar o código correto de acordo com a descrição do tipo de
     * frete para cadastro na tabela de vendas e compras.
     * @param  Array $arrFrete
     * @return Integer $codTipoFrete
     */
    public static function getCodigoFrete($arrFrete)
    {
        $codTipoFrete = $arrFrete['frt_tipo_frete'];
        if ($codTipoFrete === FRT_FRETE_CORREIOS) {

            switch (trim($arrFrete['frt_descricao'])) {
                case 'Sedex':
                    $codTipoFrete = FRT_FRETE_CORREIOS_SEDEX;
                    break;

                case 'PAC':
                    $codTipoFrete = FRT_FRETE_CORREIOS_PAC;
                    break;
            }
        }

        return (int)$codTipoFrete;
    }


    /**
     * Método responsável em retornar a descrição do frete de uma determinada venda/compra de
     * acordo com o identificador do mesmo.
     * @param  Integer $codTipoFrete
     * @return String  $descFrete
     */
    public static function getDescFrete($codTipoFrete)
    {
        $descFrete = 'Indefinido';

        if (!empty($codTipoFrete)) {

            switch ($codTipoFrete) {
                case (int)FRT_FRETE_GRATIS:
                    $descFrete = 'Frete grátis';
                    break;

                case (int)FRT_FRETE_FIXO:
                    $descFrete = 'Frete fixo';
                    break;

                case (int)FRT_FRETE_COMBINAR:
                    $descFrete = 'Frete a combinar';
                    break;

                case (int)FRT_FRETE_CORREIOS_PAC:
                    $descFrete = 'Correios (PAC)';
                    break;

                case (int)FRT_FRETE_CORREIOS_SEDEX:
                    $descFrete = 'Correios (Sedex)';
                    break;

                default:
                    $descFrete = 'Nenhum';
                    break;
            }
        }

        return $descFrete;
    }

    public static function toQtd($qtd)
    {

        $ret = '';

        if ($qtd == '') {

            $ret = '0';
        }

        $quantidade = explode('.', $qtd);

        if (count($quantidade) == 2) {

            if ((integer)$quantidade[1] == 0) {

                $ret = $quantidade[0];
            } else {

                $ret = $quantidade[0] . ',' . $quantidade[1];
            }
        }

        return $ret;
    }

    /**
     * Auxiliar Xml para array
     * @param $n
     * @return array
     */
    public static function xml2Array($n)
    {
        $return = array();
        foreach ($n->childNodes as $nc) {
            ($nc->hasChildNodes()) ? ($n->firstChild->nodeName == $n->lastChild->nodeName && $n->childNodes->length > 1)
                ? $return[$nc->nodeName][] = self::xml2Array($nc)
                : $return[$nc->nodeName] = self::xml2Array($nc)
                : $return = $nc->nodeValue;
        }

        return $return;
    }

    /**
     * Transforma xml em array utilizando biblioteca domdocument
     */
    public static function transformarXmlEmArray($retornoSinc)
    {

        $xml = new \DOMDocument();
        $xml->preserveWhiteSpace = false;
        $xml->loadXML($retornoSinc);

        $arr = self::xml2Array($xml);

        return $arr;
    }

    /**
     * Retorna a data no padrão USA sem barras até então para o sintegra
     * @param string $data
     * @return string
     */
    static public function toDateUSAoutScapes($data = '')
    {

        $data = explode(' ', $data);

        list($ano, $mes, $dia) = explode('-', $data[0]);

        return $ano . $mes . $dia;
    }

    /**
     * Retorna uma data do banco para o padrão “AAAA-MM-DD”
     * @param string $data
     * @return string
     */
    static public function toDateOutTime($data = '')
    {

        $data = explode(' ', $data);
        return $data[0];
    }

    static public function getIdObj($obj)
    {
        $arrId = (array)$obj->_id;
        return $arrId['$id'];
    }

    /**
     * Retorna a sigla para a espécie de documento de uma determinada operação.
     * @param $codEspDoc
     * @return string
     */
    static public function getAbrDescEspDoc($codEspDoc)
    {
        switch ($codEspDoc) {
            case ESP_DOC_PEDIDO:
            case ESP_DOC_PEDIDO_COMPRA:
                return 'PED';
            case ESP_DOC_NFE:
                return 'NFe';
            case ESP_DOC_CUPOM_FISCAL:
                return 'CUPOM';
            case ESP_DOC_CTE:
                return 'CTe';
            case ESP_DOC_NFSE:
                return 'NFSe';
            case ESP_DOC_SAT:
                return 'SAT';
            case ESP_DOC_NFC:
                return 'NFC';
            case ESP_DOC_INSUMO:
                return 'INS';
            case ESP_DOC_PRODUCAO:
                return 'PRO';
            default:
                return '';
        }
    }

    /**
     * passa uma string com números e retorna somente os números
     * @param $str
     * @return mixed
     */
    static public function somenteNumeros($str)
    {

        return preg_replace("/[^0-9]/", "", $str);
    }

    static public function getCodUfPorSigla($sigla)
    {
        switch ($sigla) {
            case 'AC':
                return 1;
            case 'AL':
                return 2;
            case 'AM':
                return 3;
            case 'AP':
                return 4;
            case 'BA':
                return 5;
            case 'CE':
                return 6;
            case 'DF':
                return 7;
            case 'ES':
                return 8;
            case 'GO':
                return 9;
            case 'MA':
                return 10;
            case 'MG':
                return 11;
            case 'MS':
                return 12;
            case 'MT':
                return 13;
            case 'PA':
                return 14;
            case 'PB':
                return 15;
            case 'PE':
                return 16;
            case 'PI':
                return 17;
            case 'PR':
                return 18;
            case 'RJ':
                return 19;
            case 'RN':
                return 20;
            case 'RO':
                return 21;
            case 'RR':
                return 22;
            case 'RS':
                return 23;
            case 'SC':
                return 24;
            case 'SE':
                return 25;
            case 'SP':
                return 26;
            case 'TO':
                return 27;
            default:
                return $sigla;

        }
    }

    static public function getArrayPerfisPrincipais()
    {
        return [PERFIL_ADMIN_TOTAL, PERFIL_ADMIN_VIRTUXERP, PERFIL_USUARIO_VIRTUXERP, PERFIL_CLIENTE_ECOMMERCE, PERFIL_ADMIN_FINANCA, PERFIL_ADMIN_NFE, PERFIL_ADMIN_ECOMMERCE, PERFIL_ADMIN_MOBILE];
    }

    /**
     * Retorna a desrição da unidade passada como parametro
     * @param $un
     * @return string
     */
    static public function getUnidadeDescricao($un)
    {

        $un = strtoupper($un);

        switch ($un) {

            case 'AMPOLA' :
                return 'AMPOLA';
            case 'BALDE'  :
                return 'BALDE';
            case 'BANDEJ' :
                return 'BANDEJA';
            case 'BARRA'  :
                return 'BARRA';
            case 'BISNAG' :
                return 'BISNAGA';
            case 'BLOCO'  :
                return 'BLOCO';
            case 'BOBINA' :
                return 'BOBINA';
            case 'BOMB'   :
                return 'BOMBONA';
            case 'CAPS'   :
                return 'CAPSULA';
            case 'CART'   :
                return 'CARTELA';
            case 'CENTO'  :
                return 'CENTO';
            case 'CJ'     :
                return 'CONJUNTO';
            case 'CM'     :
                return 'CENTIMETRO';
            case 'CM2'    :
                return 'CENTIMETRO QUADRADO';
            case 'CX'     :
                return 'CAIXA';
            case 'CX2'    :
                return 'CAIXA COM 2 UNIDADES';
            case 'CX3'    :
                return 'CAIXA COM 3 UNIDADES';
            case 'CX5'    :
                return 'CAIXA COM 5 UNIDADES';
            case 'CX10'   :
                return 'CAIXA COM 10 UNIDADES';
            case 'CX15'   :
                return 'CAIXA COM 15 UNIDADES';
            case 'CX20'   :
                return 'CAIXA COM 20 UNIDADES';
            case 'CX25'   :
                return 'CAIXA COM 25 UNIDADES';
            case 'CX50'   :
                return 'CAIXA COM 50 UNIDADES';
            case 'CX100'  :
                return 'CAIXA COM 100 UNIDADES';
            case 'DISP'   :
                return 'DISPLAY';
            case 'DUZIA'  :
                return 'DUZIA';
            case 'EMBAL'  :
                return 'EMBALAGEM';
            case 'FARDO'  :
                return 'FARDO';
            case 'FOLHA'  :
                return 'FOLHA';
            case 'FRASCO' :
                return 'FRASCO';
            case 'GALAO'  :
                return 'GALÃO';
            case 'GF'     :
                return 'GARRAFA';
            case 'GRAMAS' :
                return 'GRAMAS';
            case 'JOGO'   :
                return 'JOGO';
            case 'KG'     :
                return 'QUILOGRAMA';
            case 'KIT'    :
                return 'KIT';
            case 'LATA'   :
                return 'LATA';
            case 'LITRO'  :
                return 'LITRO';
            case 'M'      :
                return 'METRO';
            case 'M2'     :
                return 'METRO QUADRADO';
            case 'M3'     :
                return 'METRO CÚBICO';
            case 'MILHEI' :
                return 'MILHEIRO';
            case 'ML'     :
                return 'MILILITRO';
            case 'MWH'    :
                return 'MEGAWATT HORA';
            case 'PACOTE' :
                return 'PACOTE';
            case 'PALETE' :
                return 'PALETE';
            case 'PARES'  :
                return 'PARES';
            case 'PC'     :
                return 'PEÇA';
            case 'POTE'   :
                return 'POTE';
            case 'K'      :
                return 'QUILATE';
            case 'RESMA'  :
                return 'RESMA';
            case 'ROLO'   :
                return 'ROLO';
            case 'SACO'   :
                return 'SACO';
            case 'SACOLA' :
                return 'SACOLA';
            case 'TAMBOR' :
                return 'TAMBOR';
            case 'TANQUE' :
                return 'TANQUE';
            case 'TON'    :
                return 'TONELADA';
            case 'TUBO'   :
                return 'TUBO';
            case 'UNID'   :
                return 'UNIDADE';
            case 'VASIL'  :
                return 'VASILHAME';
            default       :
                return 'UNIDADE';
        }
    }

    /**
     * Retorna uma descrição para o código da natureza passado como parâemetro
     * @param $codNatureza
     * @return string
     */
    static public function getNaturezaDescricao($codNatureza)
    {

        switch ($codNatureza) {
            case NATUREZA_VENDA             :
                return 'VENDA';
            case NATUREZA_COMPRA            :
                return 'COMPRA';
            case NATUREZA_DEVOLUCAO_VENDA   :
                return 'DEVOLUÇÃO DE VENDA';
            case NATUREZA_ORCAMENTO         :
                return 'ORÇAMENTO';
            case NATUREZA_DEVOLUCAO_COMPRA  :
                return 'DEVOLUÇÃO DE COMPRA';
            case NATUREZA_OUTRAS_SAIDAS     :
                return 'OUTRAS SAÍDAS';
            case NATUREZA_OUTRAS_ENTRADAS   :
                return 'OUTRAS ENTRADAS';
            case NATUREZA_PRESTACAO_SERVICO :
                return 'PRESTAÇÃO DE SERVIÇOS';
            case NATUREZA_TRANSPORTE        :
                return 'TRANSPORTE';

            default :
                return 'VENDA';
        }
    }

    /**
     * Agrupa os registros por alguma chave dentro de um array
     * @param $arr
     * @param $chave
     * @return array
     */
    static public function agruparPorChave($arr, $chave, $strKeyRet = '')
    {
        $arrCod = self::transformarArray2Para1($arr, $chave);

        $arrCod = array_unique($arrCod);
        $arrNew = [];

        foreach ($arr as $reg) {
            foreach ($arrCod as $cod) {
                if ($reg[$chave] == $cod) {
                    $arrNew[$strKeyRet . $cod][] = $reg;
                }
            }
        }

        return $arrNew;
    }

    static public function getCodUfIbge($uf)
    {
        switch ($uf) {
            case 1:
                return 12;
            case 2:
                return 27;
            case 3:
                return 13;
            case 4:
                return 16;
            case 5:
                return 29;
            case 6:
                return 23;
            case 7:
                return 53;
            case 8:
                return 32;
            case 9:
                return 52;
            case 10:
                return 21;
            case 11:
                return 31;
            case 12:
                return 50;
            case 13:
                return 51;
            case 14:
                return 15;
            case 15:
                return 25;
            case 16:
                return 26;
            case 17:
                return 22;
            case 18:
                return 41;
            case 19:
                return 33;
            case 20:
                return 24;
            case 21:
                return 11;
            case 22:
                return 14;
            case 23:
                return 43;
            case 24:
                return 42;
            case 25:
                return 28;
            case 26:
                return 35;
            case 27:
                return 17;
            default:
                return $uf;

        }
    }

    static public function verificarChoqueHorario($from, $from_compare, $to, $to_compare)
    {
        $from = strtotime($from);
        $from_compare = strtotime($from_compare);
        $to = strtotime($to);
        $to_compare = strtotime($to_compare);
        $intersect = min($to, $to_compare) - max($from, $from_compare);
        if ($intersect < 0) $intersect = 0;
        $overlap = $intersect / 3600;
        if ($overlap <= 0):
            // There are no time conflicts
            return true;
        else:
            // There is a time conflict
            // echo '<p>There is a time conflict where the times overlap by ' , $overlap , ' hours.</p>';
            return false;
        endif;
    }


    /**
     * Verifica se a data é válida, caso não seja retorna uma data anterior válida.
     * @param string $strData
     * @return null|string
     */
    static public function verificarDataVencimento($strData = '')
    {
        if ($strData) {

            /**
             * date('d/m/Y', strtotime('+'.$qtdeDias.' days'));
             */

            $arrData = explode('/', $strData);

            $dia = intval($arrData[0]);
            $mes = intval($arrData[1]);
            $ano = intval($arrData[2]);

            if (!checkdate($mes, $dia, $ano)) {

                $dia = $dia - 1;
                $mes = $mes < 10 ? '0' . $mes : $mes;

                $aux = $dia . '/' . $mes . '/' . $ano;

                $strData = self::verificarDataVencimento($aux);
            }

            return $strData;
        }

        return null;
    }


    /**
     * Método responsável em criar um vetor contendo apenas e-mails válidos.
     * @param  array  $arrEmails
     * @param  string $strAttr
     * @return array  $arrRetorno
     */
    static public function setCcEmails($arrEmails, $strAttr)
    {
        $arrRetorno = [];

        if (! empty($arrEmails)) {

            foreach ($arrEmails as $email)
            {
                if (! empty($email[$strAttr])) {

                    $arrEmailsTemp = explode(';', $email[$strAttr]);
                    foreach ($arrEmailsTemp as $emailTemp)
                    {
                        $strEmail = trim($emailTemp);
                        if (filter_var($strEmail, FILTER_VALIDATE_EMAIL)) {

                            array_push($arrRetorno, $strEmail);
                        }
                    }
                }
            }
        }

        return $arrRetorno;
    }


    /**
     * Método responsável em retornar uma string contendo a seqüência de e-mails separada por vírgula.
     * @param  array  $arrEmails
     * @return string $strEmails
     */
    static public function getCcEmails($arrEmails)
    {
        $strEmails = '';

        if (! empty($arrEmails)) {

            $strEmails = trim(implode(';', $arrEmails));
        }

        return $strEmails;
    }


    /**
     * Remove o HTML de uma determinada STRING.
     * @param  string $input
     * @param  string $encoding
     * @return string $retorno
     */
    static public function noHTML($input, $encoding = 'UTF-8')
    {
        $retorno = htmlentities($input, ENT_QUOTES | ENT_HTML5, $encoding);
        $retorno = addslashes($retorno);

        return $retorno;
    }


    /**
     * Escapa uma determinada STRING para evitar SQL Injection.
     * @param  string $input
     * @return array|mixed
     */
    static public function realEscapeString($input)
    {
        if (is_array($input)) {
            return array_map(__METHOD__, $input);
        }

        if (! empty($input) && is_string($input)) {
            return str_replace(['\\', "\0", "\n", "\r", "'", '"', "\x1a"], ['\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'], $input);
        }

        return $input;
    }


    /**
     * Valida se é um CFOP usado para operações com o Exterior
     * @param $cfop
     * @return bool
     */
    static public function verificaCfopExterior($cfop)
    {
        switch ($cfop) {
            case 3101:
            case 3102:
            case 3126:
            case 3127:
            case 3201:
            case 3202:
            case 3205:
            case 3206:
            case 3207:
            case 3211:
            case 3251:
            case 3301:
            case 3351:
            case 3352:
            case 3353:
            case 3354:
            case 3355:
            case 3356:
            case 3503:
            case 3551:
            case 3553:
            case 3556:
            case 3651:
            case 3652:
            case 3653:
            case 3930:
            case 3949:
            case 7101:
            case 7102:
            case 7105:
            case 7106:
            case 7127:
            case 7201:
            case 7202:
            case 7205:
            case 7206:
            case 7207:
            case 7210:
            case 7211:
            case 7251:
            case 7301:
            case 7358:
            case 7551:
            case 7553:
            case 7556:
            case 7651:
            case 7654:
            case 7930:
            case 7949:
                return true;
                break;
        }

        return false;
    }

    /**
     * Verifica se o CFOP passado é de importação
     * @param $cfop
     * @return bool
     */
    static public function verificaCfopImportacao($cfop)
    {
        switch ($cfop) {
            case 3101:
            case 3102:
            case 3126:
            case 3127:
            case 3201:
            case 3202:
            case 3205:
            case 3206:
            case 3207:
            case 3211:
            case 3251:
            case 3301:
            case 3351:
            case 3352:
            case 3353:
            case 3354:
            case 3355:
            case 3356:
            case 3503:
            case 3551:
            case 3553:
            case 3556:
            case 3651:
            case 3652:
            case 3653:
            case 3930:
            case 3949:
                return true;
                break;
        }

        return false;
    }

    static public function getStatusPagPsico($cod)
    {


        switch ($cod):
            case STATUS_NAO_PAGO_PSICO:
                return 'Não Pago';

            case STATUS_PAGO_PSICO:
                return 'Pago';

            default:
                return 'Não Pago';
        endswitch;

    }

    /**
     * Retorna a descrição dos cargos dos usuários
     * @param $id
     * @return mixed
     */
    static public function getDescricaoCargo($id)
    {

        $arrCargos = [[
            "id" => 0,
            "label" => "Contador"
        ],[
            "id" => 1,
            "label" => "Influenciador"
        ],[
            "id" => 2,
            "label" => "Analista/Consultor/Especialista"
        ],[
            "id" => 3,
            "label" => "Financeiro"
        ],[
            "id" => 4,
            "label" => "Secretaria"
        ],[
            "id" => 5,
            "label" => "Estagiário/Assistente"
        ],[
            "id" => 6,
            "label" => "Estudante"
        ],[
            "id" => 7,
            "label" => "Outros"
        ]];
        
        return $arrCargos[$id];
    }

    /**
     * @param array $arr
     * @return array
     */
    static public function formataModToInt($arr = [])
    {
        $arrNew = [];
        foreach($arr as $r)
        {
            $arrNew[] = $r;
        }

        return $arrNew;
    }

    static public function arrayMulti2Single($array)
    {
//        if(!is_array($arrRetornoEnvio)) {
//            return;
//        }
//
//        $aResposta = [];
//
//        foreach ($arrRetornoEnvio as $k => $retorno)
//        {
//
//            if(is_array($retorno)) {
//                self::trataRetornoEnvioSefaz($retorno);
//            } else {
//                $aResposta[$k] = $retorno;
//            }
//        }
//
//        return $aResposta;

        if (!is_array($array)) {
            return false;
        }

        $result = [];
        foreach ($array as $key => $value) {

            if (is_array($value)) {

                $result = array_merge($result, self::arrayMulti2Single($value));
            } else {

                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Detarmina o código da forma de pagamento via expressão regular
        01=Dinheiro
        02=Cheque
        03=Cartão de Crédito
        04=Cartão de Débito
        05=Crédito Loja
        10=Vale Alimentação
        11=Vale Refeição
        12=Vale Presente
        13=Vale Combustível
        14=Duplicata Mercantil
        15=Boleto Bancário
        90= Sem pagamento
        99=Outros
     * @param $strFormaPgto
     * @return string
     */
    public static function getTipoPgtoNFe($strFormaPgto)
    {
        if (!empty($strFormaPgto)) {
            if (preg_match('/dinheiro/i', $strFormaPgto)) {
                return '01';
            } else if (preg_match('/cheque/i', $strFormaPgto)) {
                return '02';
            } else if (preg_match('/crédito/i', $strFormaPgto) OR preg_match('/credito/i', $strFormaPgto) OR preg_match('/cartão de crédito/i', $strFormaPgto) OR preg_match('/cartao de credito/i', $strFormaPgto)) {
                return '03';
            } else if (preg_match('/débito/i', $strFormaPgto) OR preg_match('/debito/i', $strFormaPgto) OR preg_match('/cartão de débito/i', $strFormaPgto) OR preg_match('/cartao de debito/i', $strFormaPgto)) {
                return '04';
            } else if (preg_match('/loja/i', $strFormaPgto)) {
                return '05';
            } else if (preg_match('/alimentação/i', $strFormaPgto) OR preg_match('/alimentacao/i', $strFormaPgto)) {
                return '10';
            } else if (preg_match('/refeição/i', $strFormaPgto) OR preg_match('/refeicao/i', $strFormaPgto)) {
                return '11';
            } else if (preg_match('/presente/i', $strFormaPgto)) {
                return '12';
            } else if (preg_match('/combustível/i', $strFormaPgto) OR preg_match('/combustivel/i', $strFormaPgto)) {
                return '13';
            } else if (preg_match('/duplicata/i', $strFormaPgto)) {
                return '14';
            } else if (preg_match('/boleto/i', $strFormaPgto)) {
                return '15';
            }
        }

        return '99';
    }
    
    public static function getStringEndereco($arrEnd)
    {

        $strEnd = '';
        if ($arrEnd) {

            if ($arrEnd['end_endereco']) {

                $strEnd .= $arrEnd['end_endereco'] . ', ';
            }

            if ($arrEnd['end_endereco_nro']) {

                $strEnd .= $arrEnd['end_endereco_nro'] . ', ';
            }

            if ($arrEnd['end_endereco_bairro']) {

                $strEnd .= $arrEnd['end_endereco_bairro'] . '. ';
            }

            if ($arrEnd['end_endereco_cidade']) {

                $strEnd .= $arrEnd['end_endereco_cidade'] . '/';
            }

            if ($arrEnd['end_endereco_uf']) {

                $strEnd .= $arrEnd['end_endereco_uf'];
            }
        }


        return $strEnd;
    }

    public static function getDescricaoTipoContingencia($cod)
    {

        switch ($cod)
        {

            case FORMA_EMISSAO_CONTINGENCIA_DPEC:
                return 'EPEC';
                break;

            case FORMA_EMISSAO_CONTINGENCIA_SVC_AN:
                return 'SVCAN';
                break;

            case FORMA_EMISSAO_FORMULARIO_SEGURANCA_FS_DA:
                return 'FSDA';
                break;

            case FORMA_EMISSAO_CONTINGENCIA_SVC_RS:
                return 'SVCRS';
                break;
        }
    }

    public static function debug($arr)
    {

        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }

    public static function excluiCamposInterface($nome)
    {

        switch ($nome)
        {
            case 'NFe':
            case 'TRetEnviNFe':
            case 'tpAmb':
            case 'TNFe':
                return true;
                break;
            default:
                return false;
        }
    }

    public static function verificaCamposHeaderInterface($nome)
    {

        switch ($nome)
        {
            case 'dest':
            case 'infNFe':
            case 'det':
            case 'prod':
            case 'DI':
            case 'adi':
            case 'exportInd':
            case 'rastro':
            case 'imposto':
            case 'retTrib':
            case 'transp':
            case 'cobr':
            case 'fat':
            case 'pag':
            case 'card':
            case 'exporta':
            case 'compra':
            case 'cana':
            case 'safra':
            case 'deduc':
            case 'NFref':
                return true;
                break;
            default:
                return false;
        }
    }
}