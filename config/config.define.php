<?php

include_once 'conexao.php';

$httpHost = @$_SERVER['HTTP_HOST'];
$serverName = @$_SERVER['SERVER_NAME'];
$requestUri = @$_SERVER['REQUEST_URI'];

define('URL_ATUAL',HTTP.'://'.$serverName.$requestUri.'/');
//die(URL_ATUAL);
define('URL_STORAGE','/var/www/storage/');

//*******************************************************************
//define de MSGs
define('TIT_SUCCESS','Sucesso!');
define('MSG_CADASTRO_SUCCESS','Dados cadastrados!');
define('MSG_ATUALIZACAO_SUCCESS','Dados atualizados!');
define('TIT_ERROR','Erro!');
define('MSG_ERROR','Houve algum problema ao salvar os dados.');
define('MSG_EXCLUSAO_SUCCESS','Registro(s) excluído(s)!');
define('MSG_ERROR_SELECT','Houve algum problema ao buscar os dados.');

define('SUCESSO','1');
define('ERRO','0');

/*************************************************
 * DEFINE DE NATUREZAS
 *************************************************/
define('NATUREZA_VENDA','1');
define('NATUREZA_COMPRA','2');
define('NATUREZA_DEVOLUCAO_VENDA','3');
define('NATUREZA_ORCAMENTO','11');
define('NATUREZA_DEVOLUCAO_COMPRA','4');
define('NATUREZA_OUTRAS_SAIDAS','5');
define('NATUREZA_OUTRAS_ENTRADAS','6');

/*************************************************
 * DEFINE DE TIPO PARCELAMENTO
 *************************************************/
define('PARC_AVISTA','0');
define('PARC_APRAZO','1');

/*************************************************
 * DEFINE OS TIPOS ESPECÍFICOS DE PRODUTOS
 *************************************************/
define('PRO_TIPO_NORMAL'     , '0');
define('PRO_TIPO_VEICULO'    , '1');
define('PRO_TIPO_MEDICAMENTO', '2');
define('PRO_TIPO_ARMAS'      , '3');
define('PRO_TIPO_COMBUSTIVEL', '4');

/*************************************************
 * DEFINE DOS TIPOS DE IMPOSTOS
 *************************************************/
define('TRIB_ICMS_PROPRIO','1');
define('TRIB_ICMS_ST','2');
define('TRIB_ICMS_INTERESTADUAL','3');
define('TRIB_ICMS_PARTILHA','4');
define('TRIB_ICMS_PARTILHA_ORIGEM','5');
define('TRIB_ICMS_PARTILHA_DESTINO','6');
define('TRIB_ISS','10');
define('TRIB_IPI','11');
define('TRIB_PIS','12');
define('TRIB_COFINS','13');
define('TRIB_IR','14');
define('TRIB_II','15');
define('TRIB_INSS','16');
define('TRIB_CSLL','17');
define('TRIB_FCP','18');
define('TRIB_FCP_ST','19');

/*************************************************
 * DEFINE DOS TIPOS DE NÃO TRIBUTADO
 *************************************************/
define('TRI_NAOTRIB_TIP_ISENTO','1');
define('TRI_NAOTRIB_TIP_NAO_TRIBUTADO','2');
define('TRI_NAOTRIB_TIP_OUTRAS','3');

/*************************************************
 * DEFINE DAS OPERACOES SEFAZ
 *************************************************/
define('OPERACAO_SEFAZ_AUTORIZAR','1');
define('OPERACAO_SEFAZ_CANCELAR','2');
define('OPERACAO_SEFAZ_CCE','3');
define('OPERACAO_SEFAZ_INUTILIZAR','4');
define('OPERACAO_SEFAZ_CONSULTAR','5');




/*************************************************
 * DEFINE DAS FORMAS DE EMISSOES SEFAZ
 *************************************************/
define('FORMA_EMISSAO_NORMAL','1');
define('FORMA_EMISSAO_CONTINGENCIA_FORMULARIO_SEGURANCA','2');
define('FORMA_EMISSAO_CONTINGENCIA_SCAN','3');
define('FORMA_EMISSAO_CONTINGENCIA_DPEC','4');
define('FORMA_EMISSAO_FORMULARIO_SEGURANCA_FS_DA','5');
define('FORMA_EMISSAO_CONTINGENCIA_SVC_AN','6');
define('FORMA_EMISSAO_CONTINGENCIA_SVC_RS','7');

/*************************************************
 * DEFINE DAS ESPECIE DE DOCUMENTOS
 *************************************************/
define('ESP_DOC_NFE','55');
define('ESP_DOC_NFC','65');

/*************************************************
 * DEFINE DOS TIPOS DE FINALIDADE DA NFE
 *************************************************/
define('FINALIDADE_NORMAL','1');
define('FINALIDADE_COMPLEMENTAR','2');
define('FINALIDADE_AJUSTE','3');
define('FINALIDADE_DEVOLUCAO','4');

/*************************************************
 * DEFINE DOS TIPO DE CONTRIBUINTE
 *************************************************/
define('TIP_CONTRIBUINTE_ICMS','1');
define('TIP_CONTRIBUINTE_ISENTO','2');
define('TIP_CONTRIBUINTE_CONSUMIDOR','9');
define('TIP_RETEM_ISS','1');

/*************************************************
 * DEFINE DOS REGIMES TRIBUTARIOS
 *************************************************/
define('REG_TRIB_MEI','1');
define('REG_TRIB_EPP','2');
define('REG_TRIB_ME','3');
define('REG_TRIB_LR','4');
define('REG_TRIB_LP','5');
define('REG_TRIB_DB','6');

/*************************************************
 * DEFINE DOS TIPO DE PESSOA
 *************************************************/
define('TIP_PESSOA_FISICA','1');
define('TIP_PESSOA_JURIDICA','2');

define('INSERIR','inserir');
define('ATUALIZAR','atualizar');
define('EXCLUIR','excluir');
define('CANCELAR','cancelar');
define('FATURAR','faturar');
define('SELECIONAR','selecionar');

/*************************************************
 * DEFINE DOS STATUS DE SITUACAO
 *************************************************/
define('NFE_PENDENTE_ENVIO','0');
define('NFE_ALERTA','12');
define('NFE_AGUARDANDO','13');
define('NFE_ERRO','14');
define('NFE_EPEC','15');
define('NFE_INUTILIZADO','81');
define('NFE_AUTORIZADO','90');
define('NFE_DENEGADO','91');
define('NFE_ENCERRADO','92');
define('NFE_CANCELADO','99');

/*************************************************
 * DEFINE DOS TIPOS DE BUSCA RECEITA
 *************************************************/
define('NFE_BUSCA_POR_RECIBO','1');
define('NFE_BUSCA_POR_CHAVE','2');

/*************************************************
 * DEFINE DOS AMBIENTES DE ENVIO RECEITA
 *************************************************/
define('NFE_AMBIENTE_HOMOLOGACAO','2');
define('NFE_AMBIENTE_PRODUCAO','1');

define('QUEBRA_LINHA', "\r\n");

define('EXTRAS_DIR', NFE_DIR.'/extras');