<?php

use Phalcon\DI\FactoryDefault as DefaultDI,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Logger,
    EmissorNfe\Helpers\Log as Log,
    EmissorNfe\Helpers\AuthToken as AuthToken,
    Phalcon\Logger\Adapter\File as FileLogger,
	Phalcon\Loader;

require_once('vendor/autoload.php');

if (!function_exists('getallheaders'))
{
    $headers = '';
    $str = '';

    $str = 'uri: ' .$_SERVER['REQUEST_URI'].'<br>';


    foreach ($_SERVER as $name => $value)
    {

        if (substr($name, 0, 5) == 'HTTP_') {

            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }

}

$arr = explode('/',$_SERVER['REQUEST_URI']);
if(!isset($arr[2]) OR $arr[2] == 'undefined' OR empty($arr[2])) {
    exit;
}

/**
 * By default, namespaces are assumed to be the same as the path.
 * This function allows us to assign namespaces to alternative folders.
 * It also puts the classes into the PSR-0 autoLoader.
 */

$loader = new Loader();

$loader->registerNamespaces(array(
	'EmissorNfe\Controllers' => __DIR__ . '/app/controllers/',
	'EmissorNfe\Exceptions' => __DIR__ . '/exceptions/',
    'EmissorNfe\Helpers' => __DIR__ . '/library/helpers/',
    'EmissorNfe\Helpers\Nfe' => __DIR__ . '/library/helpers/nfe',
    'OrganicRest\Responses' => __DIR__ . '/library/responses/',
    'OrganicRest\Responses\Schemas' => __DIR__ . '/library/responses/schemas/',
    'AsyncPHP\Doorman\Manager\ProcessManager' => __DIR__ . '/library/doorman/src/Manager',
));

//var_dump(__DIR__ . '/library/doorman/src/Task');die;

require_once __DIR__ . '/config/config.php';
require_once 'config/config.define.php';

$loader->registerDirs(
    array(
         __DIR__ . '/app/controllers/'
        )
);

$loader->register();

/**
 * The DI is our direct injector.  It will store pointers to all of our services
 * and we will insert it into all of our controllers.
 * @var DefaultDI
 */
$di = new DefaultDI();

$di->set('dispatcher', function() {
    $dispatcher = new Dispatcher();
    $dispatcher->setDefaultNamespace('EmissorNfe\Erp\Controllers');
    return $dispatcher;
});


/**
 * Return array of the Collections, which define a group of routes, from
 * routes/collections.  These will be mounted into the app itself later.
 */



//\EmissorNfe\Helpers\Log::setLog('iniciandoooo....');
//\EmissorNfe\Helpers\Log::setLog('$_SERVER: '.print_r($_SERVER,true));


$config = new \Phalcon\Config($config);

$di->set('collections', function(){
    return include('./routes/routeLoader.php');
});

/**
 * $di's setShared method provides a singleton instance.
 * If the second parameter is a function, then the service is lazy-loaded
 * on its first instantiation.
 */
$di->setShared('config', $config);

// As soon as we request the session service, it will be started.
$di->setShared('session', function(){

	$session = new \Phalcon\Session\Adapter\Files();
	$session->start();
	return $session;
});

$di->set('modelsCache', function() {

	//Cache data for one day by default
	$frontCache = new \Phalcon\Cache\Frontend\Data(array(
		'lifetime' => 3600
	));

	//File cache settings
	$cache = new \Phalcon\Cache\Backend\File($frontCache, array(
		'cacheDir' => __DIR__ . '/cache/'
	));

	return $cache;
});

// Registering the collectionManager service
$di->set('collectionManager', function(){
    return new Phalcon\Mvc\Collection\Manager();
}, true);

/**
 * Database setup.  Here, we'll use a simple SQLite database of Disney Princesses.
 */

//$di->set('db', function() use ($config) {
//
//    $eventsManager = new Phalcon\Events\Manager();
//
//    if(AMBIENTE_GERAL == 'DESENVOLVIMENTO') {
//
//            $logger = new FileLogger("/var/www/db.log");
//
//        $logger->log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', Logger::INFO);
//
//        //Listen all the database events
//        $eventsManager->attach('db', function($event, $connection) use ($logger) {
//
//            $logger->log(print_r($connection,true), Logger::INFO);
//
//           // if ($event->getType() == 'beforeQuery') {
//                $logger->log($connection->getSQLStatement() ."\n" .'aaaa:' .print_r($connection->getSQLVariables(),true). print_r($connection->getSQLBindTypes(),true), Logger::INFO);
//
//    //            $profiler->startProfile($connection->getSQLStatement(), $connection->getSQLVariables(), $connection->getSQLBindTypes());
//           // }
//        });
//    }
//
//    $connection = new \Phalcon\Db\Adapter\Pdo\Postgresql(array(
//        'host' =>  $config->db->host,
//        'username' => $config->db->username,
//        'password' => $config->db->password,
//        //'port' => '5433',
//        'dbname' => $config->dbNameVirtux
//    ));
//
//    //Assign the eventsManager to the db adapter instance
//    $connection->setEventsManager($eventsManager);
//
//    return $connection;
//
//});

//$di->set('dbSistema', function() use ($config){
//
//    $eventsManager = new Phalcon\Events\Manager();
//
//   // $logger = new FileLogger("/var/www/db.log");
//
//    $connection = new \Phalcon\Db\Adapter\Pdo\Postgresql(array(
//        'host' => $config->dbSistema->host,
//        'username' => $config->dbSistema->username,
//        'password' => $config->dbSistema->password,
//        'dbname' => $config->dbSistema->dbname,
//    ));
//
//    //Listen all the database events
////    $eventsManager->attach('dbSistema', function($event, $connection) use ($logger) {
//
////        if ($event->getType() == 'beforeQuery') {
////            $logger->log($connection->getSQLStatement(), Logger::INFO);
////        }
////    });
//
//    //Assign the eventsManager to the db adapter instance
//    $connection->setEventsManager($eventsManager);
//
//
//    return $connection;
//
//});

/**
 * If our request contains a body, it has to be valid JSON.  This parses the
 * body into a standard Object and makes that vailable from the DI.  If this service
 * is called from a function, and the request body is nto valid JSON or is empty,
 * the program will throw an Exception.
 */
$di->setShared('requestBody', function() {
//    die('jkjkj');

    //\EmissorNfe\Helpers\Log::setLog('requestBody');
	$in = file_get_contents('php://input');

    //\EmissorNfe\Helpers\Log::setLog('php://input: '.print_r($in,true));

	$in = json_decode($in, false);

	// JSON body could not be parsed, throw exception
	if($in === null){
		throw new HTTPException(
			'There was a problem understanding the data sent to the server by the application.',
			409,
			array(
				'dev' => 'The JSON body sent to the server was unable to be parsed.',
				'internalCode' => 'REQ1000',
				'more' => ''
			)
		);
	}

	return $in;
});

$di->set('getHttpRequest', function(){

    //\EmissorNfe\Helpers\Log::setLog('getHttpRequest');
    $request = new \Phalcon\Http\Request();

    //\EmissorNfe\Helpers\Log::setLog('uri: '.print_r($request->getHeaders(),true));

    return $request->getHeaders();


}, true);

$di->set('arrUrl',function() use ($di) {

    $arr = explode('/',$_SERVER['REQUEST_URI']);

    $arrUrl['mod'] = $arr[2];
    $arrUrl['controller'] = $arr[3];

    return $arrUrl;

},true);

/**
 * Out application is a Micro application, so we mush explicitly define all the routes.
 * For APIs, this is ideal.  This is as opposed to the more robust MVC Application
 * @var $app
 */
$app = new Phalcon\Mvc\Micro();
$app->setDI($di);

$app->before(function() use ($app) {

    Log::setLog(['desc' => 'REQUEST_URI:'.$_SERVER['REQUEST_URI']]);
    //Log::setLog(['desc' => 'fffffffff: ' . print_r($app->di->get('getDecodedJwt'),true)]);



    //Log::setLog('SERVER TODOs:'.print_r($_SERVER,true));

    //die($app->getRouter()->getRewriteUri());

});



/**
 * Mount all of the collections, which makes the routes active.
 */
foreach($di->get('collections') as $collection)
{
    $app->mount($collection);
}


/**
 * The base route return the list of defined routes for the application.
 * This is not strictly REST compliant, but it helps to base API documentation off of.
 * By calling this, you can quickly see a list of all routes and their methods.
 */
$app->get('/', function() use ($app){
    //die('oi');
    //\EmissorNfe\Helpers\Log::setLog('get /');
	$routes = $app->getRouter()->getRoutes();
	$routeDefinitions = array('GET'=>array(), 'POST'=>array(), 'PUT'=>array(), 'PATCH'=>array(), 'DELETE'=>array(), 'HEAD'=>array(), 'OPTIONS'=>array());
	foreach($routes as $route)
    {
        //print_r($route);
		$method = $route->getHttpMethods();
		$routeDefinitions[$method][] = $route->getPattern();
	}
	return $routeDefinitions;
});

/**
 * After a route is run, usually when its Controller returns a final value,
 * the application runs the following function which actually sends the response to the client.
 *
 * The default behavior is to send the Controller's returned value to the client as JSON.
 * However, by parsing the request querystring's 'type' paramter, it is easy to install
 * different response type handlers.  Below is an alternate csv handler.
 */
$app->after(function() use ($app) {

    //\EmissorNfe\Helpers\Log::setLog('afterrrrrrrr');

	// OPTIONS have no body, send the headers, exit
	if($app->request->getMethod() == 'OPTIONS'){
		$app->response->setStatusCode('200', 'OK');
		$app->response->send();
       // \EmissorNfe\Helpers\Log::setLog('entrou no AFTER if(options)');
		return;
	}

	// Respond by default as JSON
	if(!$app->request->get('type') || $app->request->get('type') == 'json'){

		// Results returned from the route's controller.  All Controllers should return an array
		$records = $app->getReturnedValue();

		$response = new \OrganicRest\Responses\JSONResponse();
		$response->useEnvelope(false) //this is default behavior
			->convertSnakeCase(true) //this is also default behavior
			->send($records);

		return;
	}
	else if($app->request->get('type') == 'csv'){

		$records = $app->getReturnedValue();
		$response = new \EmissorNfe\Responses\CSVResponse();
		$response->useHeaderRow(true)->send($records);

		return;
	}
	else {
//        die('aaaa');
		throw new \EmissorNfe\Exceptions\HTTPException(
			'Could not return results in specified format',
			403,
			array(
				'dev' => 'Could not understand type specified by type paramter in query string.',
				'internalCode' => 'NF1000',
				'more' => 'Type may not be implemented. Choose either "csv" or "json"'
			)
		);
	}
});

/**
 * The notFound service is the default handler function that runs when no route was matched.
 * We set a 404 here unless there's a suppress error codes.
 */
//echo '<pre>';
//print_r($app);

$app->notFound(function () use ($app) {
    \EmissorNfe\Helpers\Log::setLog('notFounddddd');
	throw new \EmissorNfe\Exceptions\HTTPException(
		'Not Found.',
		404,
		array(
			'dev' => 'That route was not found on the server: ' . $_SERVER['REQUEST_URI'],
			'internalCode' => 'NF1000',
			'more' => 'Check route for mispellings.',
		)
	);
});

//$app->error(function ($exception) use ($app) {
//
//    $f = fopen('/var/www/erro.txt','a+');
//    fwrite($f,date('H:i:s').' --> error: ' .print_r($exception,true));
//    fclose($f);
//});

/**
 * If the application throws an HTTPException, send it on to the client as json.
 * Elsewise, just log it.
 * TODO:  Improve this.
 */



set_exception_handler(function($exception) use ($app){

//    $f = fopen('/var/www/erro.txt','a+');
//    fwrite($f,date('H:i:s').' --> ' .print_r($exception,true));
//    fclose($f);

    if(isset($_SERVER['REQUEST_URI'])) {
        $arr = explode('/',$_SERVER['REQUEST_URI']);

    } else {
        $arr[1] = 'vteste';
    }

//    \EmissorNfe\Helpers\Log::setLog('metodo set_exception_handler: ' .  print_r($exception,true));
//    print_r($exception);die;
	//HTTPException's send method provides the correct response headers and body

   // Log::setLog('$exc11: '. print_r($exception,true));

    if(is_a($exception, 'Firebase\\JWT\\ExpiredException')){

        Log::salvarLog(['desc' => 'Erro de token expirado ocorrido com a requisição: '.$_SERVER['REQUEST_URI'] . ' exception: '.$exception->getMessage(),
                        'modulo' => MOD_ERRO_ACESSO_API,
                        'solucao' => SOL_API]);
//
        //$exception->send();
//
        return;
    }


    if(is_a($exception, 'EmissorNfe\\Exceptions\\HTTPException')){

        Log::salvarLog(['desc' => 'Erro generico ocorrido com a requisição: '.$_SERVER['REQUEST_URI'] . ' exception: '.$exception->getMessage()]);

        $exception->send();

        return;
	}

    if(is_a($exception, 'NFePHP\\Common\\Exception\\RuntimeException')) {

//        Log::setLog('cai no iff exception do nfephp: ' . print_r($exception,true));
    
        //Log::setLog('adsfaaaaaaa:' . $exception->getMessage());

        $objMNota = new \EmissorNfe\Erp\Models\MNota();

        $arrInserirNota = [
            'cta_cod_cta' => $arr[3],
            'tip_nota' => SOL_NFE,
            'msg' => $exception->getMessage()
        ];

        $objMNota->salvarNota($arrInserirNota);
    }



	error_log($exception);
	error_log($exception->getTraceAsString());
});

//$error_list = array();

$myErrorHandler = function ($error_level, $error_message, $error_file, $error_line, $error_context) use ($app) {
    $error_list[] = $error_level;


//    $f = fopen('/var/www/erro.txt','a+');
//    fwrite($f,date('H:i:s').' --> '.$_SERVER['REQUEST_URI'] . ' -- > ' .print_r($error_list,true) .' ------------ '. print_r($error_message,true) ."\n\r\n\r");
//    fclose($f);


    // Tell PHP to also run its error handler
    return false;
};
// Set your own error handler
$old_error_handler = set_error_handler($myErrorHandler);

$app->handle();